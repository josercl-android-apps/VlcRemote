package josercl.vlcremote.di

import android.content.Context
import androidx.room.Room
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import josercl.vlcremote.db.VlcRemoteDatabase
import josercl.vlcremote.model.Favorite
import josercl.vlcremote.model.mapper.EntityDomainMapper
import josercl.vlcremote.model.mapper.FavoriteMapper
import javax.inject.Singleton
import josercl.vlcremote.db.entity.Favorite as DbFavorite

@Module
@InstallIn(SingletonComponent::class)
abstract class DatabaseModule {
    companion object {
        @Provides
        @Singleton
        fun getRoomDatabase(@ApplicationContext context: Context): VlcRemoteDatabase {
            return Room.databaseBuilder(context, VlcRemoteDatabase::class.java, "vlc-remote")
                .fallbackToDestructiveMigration()
                .build()
        }

        @Provides
        @Singleton
        fun getFavoritesDao(db: VlcRemoteDatabase) = db.favoriteDao()

        @Provides
        @Singleton
        fun providesFavoriteMapper() = FavoriteMapper()
    }

    @Binds
    abstract fun bindFavoriteMapper(mapper: FavoriteMapper): EntityDomainMapper<DbFavorite, Favorite>
}