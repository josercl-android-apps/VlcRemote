package josercl.vlcremote.di

import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import josercl.vlcremote.CoroutineContextDispatcher
import josercl.vlcremote.DefaultCoroutineContextDispatcher
import josercl.vlcremote.net.Scanner
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ContextModule {
    @Provides
    @Singleton
    fun getSharedPreferences(@ApplicationContext context: Context): SharedPreferences =
        PreferenceManager.getDefaultSharedPreferences(context)

    @Provides
    @Singleton
    fun providesNetworkScanner(@ApplicationContext context: Context) = Scanner(context)

    @Provides
    @Singleton
    fun providesCoroutineContextDispatcher(): CoroutineContextDispatcher
        = DefaultCoroutineContextDispatcher()
}