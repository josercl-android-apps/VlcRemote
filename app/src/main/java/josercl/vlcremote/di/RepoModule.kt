package josercl.vlcremote.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import josercl.vlcremote.db.dao.FavoriteDao
import josercl.vlcremote.db.repository.FavoriteRepository
import josercl.vlcremote.model.mapper.EntityDomainMapper
import kotlinx.coroutines.ExperimentalCoroutinesApi
import josercl.vlcremote.db.entity.Favorite as DbFavorite
import josercl.vlcremote.model.Favorite as ModelFavorite

@ExperimentalCoroutinesApi
@Module
@InstallIn(ViewModelComponent::class)
object RepoModule {
    @Provides
    fun providesFavoriteRepository(
        dao: FavoriteDao,
        mapper: EntityDomainMapper<DbFavorite, ModelFavorite>
    ) = FavoriteRepository(dao, mapper)
}