package josercl.vlcremote.di

import android.content.SharedPreferences
import com.squareup.moshi.Moshi
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import josercl.vlcremote.model.json.AudioFiltersAdapter
import josercl.vlcremote.model.json.BooleanAdapter
import josercl.vlcremote.model.json.CategoryAdapter
import josercl.vlcremote.model.json.EqualizerAdapter
import josercl.vlcremote.net.IVlcService
import josercl.vlcremote.net.VlcClientManager
import josercl.vlcremote.net.VlcService
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class NetworkModule {
    companion object {
        @Provides
        @Singleton
        fun getMoshi(): Moshi {
            return Moshi.Builder()
                .add(BooleanAdapter())
                .add(EqualizerAdapter())
                .add(CategoryAdapter())
                .add(AudioFiltersAdapter())
                .build()
        }

        @Provides
        @Singleton
        fun getVlcClientManager(moshi: Moshi, preferences: SharedPreferences) =
            VlcClientManager(moshi, preferences)

        @Provides
        @Singleton
        fun getApiService(vlcClientManager: VlcClientManager) = VlcService(vlcClientManager)
    }

    @Binds
    abstract fun bindVlcService(vlcService: VlcService): IVlcService
}