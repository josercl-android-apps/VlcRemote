package josercl.vlcremote

import kotlinx.coroutines.Dispatchers
import kotlin.coroutines.CoroutineContext

interface CoroutineContextDispatcher {
    val IO: CoroutineContext
    val Main: CoroutineContext
}

class DefaultCoroutineContextDispatcher: CoroutineContextDispatcher {
    override val IO: CoroutineContext = Dispatchers.IO
    override val Main: CoroutineContext = Dispatchers.Main
}