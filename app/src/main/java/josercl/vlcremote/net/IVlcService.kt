package josercl.vlcremote.net

import josercl.vlcremote.model.enum.VlcSortMode
import josercl.vlcremote.model.enum.VlcSortOrder
import josercl.vlcremote.model.net.BrowseResponse
import josercl.vlcremote.model.net.EqualizerStatus
import josercl.vlcremote.model.net.PlaylistNode
import josercl.vlcremote.model.net.StatusResponse

interface IVlcService {
    suspend fun getStatus(): StatusResponse

    suspend fun play(): StatusResponse

    suspend fun pause(): StatusResponse

    suspend fun stop(): StatusResponse

    suspend fun previous(): StatusResponse

    suspend fun next(): StatusResponse

    suspend fun repeat(): StatusResponse

    suspend fun random(): StatusResponse

    suspend fun clearPlaylist(): StatusResponse

    suspend fun setFullScreen(): StatusResponse

    suspend fun seekTo(value: String): StatusResponse

    suspend fun setVolume(value: Int): StatusResponse

    suspend fun setSubtitle(value: Int): StatusResponse

    suspend fun openFile(uri: String): StatusResponse

    suspend fun enqueue(uri: String): StatusResponse

    suspend fun playItemFromPlaylist(id: Int): StatusResponse

    suspend fun removeItemFromPlaylist(id: Int): StatusResponse

    suspend fun sortPlaylist(order: VlcSortOrder, mode: VlcSortMode): StatusResponse

    suspend fun setEqualizerEnabled(status: EqualizerStatus): StatusResponse

    suspend fun setEqualizerPreset(preset: Int): StatusResponse

    suspend fun setEqualizerPreamp(preamp: Double): StatusResponse

    suspend fun setEqualizerBand(band: Int, value: Double): StatusResponse

    suspend fun setSubtitleDelay(delay: Double): StatusResponse

    suspend fun browse(dir: String): BrowseResponse

    suspend fun getPlaylist(): PlaylistNode
}