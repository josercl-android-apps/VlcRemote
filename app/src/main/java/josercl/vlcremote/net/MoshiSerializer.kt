package josercl.vlcremote.net

import com.squareup.moshi.Moshi
import io.ktor.client.features.json.JsonSerializer
import io.ktor.http.ContentType
import io.ktor.http.content.OutgoingContent
import io.ktor.http.content.TextContent
import io.ktor.util.reflect.TypeInfo
import io.ktor.utils.io.core.Input
import io.ktor.utils.io.core.readText

class MoshiSerializer(private val moshi: Moshi) : JsonSerializer {
    override fun write(data: Any, contentType: ContentType): OutgoingContent {
        return TextContent(moshi.adapter(data.javaClass).toJson(data), contentType)
    }

    override fun read(type: TypeInfo, body: Input): Any {
        val text = body.readText()
        return checkNotNull(moshi.adapter(type.type.javaObjectType).fromJson(text)) {
            "Parsing a Value to null is not allowed"
        }
    }
}