package josercl.vlcremote.net

import josercl.vlcremote.model.enum.VlcCommand
import josercl.vlcremote.model.enum.VlcSortMode
import josercl.vlcremote.model.enum.VlcSortOrder
import josercl.vlcremote.model.net.BrowseResponse
import josercl.vlcremote.model.net.EqualizerStatus
import josercl.vlcremote.model.net.PlaylistNode

class VlcService (
    private val manager: VlcClientManager
) : IVlcService {

    private fun getApi() = manager.getClient()

    override suspend fun getStatus() = getApi().status()

    override suspend fun play() = getApi().status(VlcCommand.PLAY)

    override suspend fun pause() = getApi().status(VlcCommand.PAUSE)

    override suspend fun stop() = getApi().status(VlcCommand.STOP)

    override suspend fun previous() = getApi().status(VlcCommand.PREV)

    override suspend fun next() = getApi().status(VlcCommand.NEXT)

    override suspend fun repeat() = getApi().status(VlcCommand.REPEAT)

    override suspend fun random() = getApi().status(VlcCommand.RANDOM)

    override suspend fun clearPlaylist() = getApi().status(VlcCommand.CLEAR_PLAYLIST)

    override suspend fun setFullScreen() = getApi().status(VlcCommand.FULLSCREEN)

    override suspend fun seekTo(value: String) =
        getApi().status(VlcCommand.SEEK, mapOf("val" to value))

    override suspend fun setVolume(value: Int) =
        getApi().status(VlcCommand.VOLUME, mapOf("val" to "$value"))

    override suspend fun setSubtitle(value: Int) =
        getApi().status(VlcCommand.SUBTITLE, mapOf("val" to "$value"))

    override suspend fun openFile(uri: String) =
        getApi().status(VlcCommand.OPEN, mapOf("input" to uri))

    override suspend fun enqueue(uri: String) =
        getApi().status(VlcCommand.ENQUEUE, mapOf("input" to uri))

    override suspend fun playItemFromPlaylist(id: Int) =
        getApi().status(VlcCommand.PLAY, mapOf("id" to "$id"))

    override suspend fun removeItemFromPlaylist(id: Int) =
        getApi().status(VlcCommand.DELETE, mapOf("id" to "$id"))

    override suspend fun sortPlaylist(order: VlcSortOrder, mode: VlcSortMode) =
        getApi().status(VlcCommand.SORT, mapOf("id" to order.toString(), "val" to mode.toString()))

    override suspend fun setEqualizerEnabled(status: EqualizerStatus) =
        getApi().status(VlcCommand.ENABLE_EQ, mapOf("val" to status.toString()))

    override suspend fun setEqualizerPreset(preset: Int) =
        getApi().status(VlcCommand.PRESET, mapOf("val" to "$preset"))

    override suspend fun setEqualizerPreamp(preamp: Double) =
        getApi().status(VlcCommand.PREAMP, mapOf("val" to "$preamp"))

    override suspend fun setEqualizerBand(band: Int, value: Double) =
        getApi().status(VlcCommand.EQUALIZER, mapOf("val" to "$value", "band" to "$band"))

    override suspend fun setSubtitleDelay(delay: Double) =
        getApi().status(VlcCommand.SUBTITLE_DELAY, mapOf("val" to "$delay"))

    override suspend fun browse(dir: String): BrowseResponse = getApi().browse(dir)

    override suspend fun getPlaylist(): PlaylistNode = getApi().getPlaylist()
}