package josercl.vlcremote.net

import android.content.Context
import android.net.ConnectivityManager
import android.net.LinkProperties
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import android.net.wifi.WifiManager
import android.os.Build
import androidx.activity.ComponentActivity
import josercl.vlcremote.extensions.inc
import josercl.vlcremote.extensions.toInetAddress
import josercl.vlcremote.extensions.toPositiveInt
import kotlinx.coroutines.suspendCancellableCoroutine
import java.math.BigInteger
import java.net.Inet4Address
import java.net.InetAddress
import java.net.NetworkInterface
import java.net.UnknownHostException
import java.nio.ByteOrder
import kotlin.coroutines.resume
import kotlin.math.pow

class Scanner(private val context: Context) {

    private lateinit var deviceAddr: InetAddress
    private var networkAddress = intArrayOf(0, 0, 0, 0).toInetAddress()
    private var hostsQty: Int = 0
    private lateinit var broadcast: InetAddress

    private val inetAddressZero = InetAddress.getByAddress(byteArrayOf(0, 0, 0, 0))

    suspend fun getIpRange(): List<InetAddress> {
        getDeviceIpAddr()

        if (deviceAddr == inetAddressZero) {
            return emptyList()
        }

        init()

        var newAddr = networkAddress
        val totalHost = hostsQty

        val ips = mutableListOf<InetAddress>()

        (0..totalHost).forEach { _ ->
            newAddr++
            if (newAddr != broadcast) {
                ips += newAddr
            }
        }

        return ips
    }

    private suspend fun getDeviceIpAddr() {
        deviceAddr = suspendCancellableCoroutine { cont ->
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.S) {
                cont.resume(legacyGetDeviceIpAddr())
            } else {
                val connectivityManager = context.getSystemService(ConnectivityManager::class.java)

                val request = NetworkRequest.Builder()
                    .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
                    .build()

                val callback = object : ConnectivityManager.NetworkCallback() {
                    override fun onLinkPropertiesChanged(
                        network: Network,
                        linkProperties: LinkProperties
                    ) {
                        super.onLinkPropertiesChanged(network, linkProperties)
                        val first = linkProperties.linkAddresses
                            .first { it.address.isSiteLocalAddress }
                        cont.resume(first.address)

                        connectivityManager.unregisterNetworkCallback(this)
                    }
                }

                connectivityManager.registerNetworkCallback(request, callback)
            }
        }
    }

    @Suppress("DEPRECATION")
    private fun legacyGetDeviceIpAddr(): InetAddress {
        val wifiManager =
            context.applicationContext.getSystemService(ComponentActivity.WIFI_SERVICE) as WifiManager
        var ipAddressInt = wifiManager.connectionInfo.ipAddress

        if (ByteOrder.nativeOrder() == ByteOrder.LITTLE_ENDIAN) {
            ipAddressInt = Integer.reverseBytes(ipAddressInt)
        }

        val ipByteArray = BigInteger.valueOf(ipAddressInt.toLong()).toByteArray()

        return try {
            InetAddress.getByAddress(ipByteArray)
        } catch (e: UnknownHostException) {
            inetAddressZero
        }
    }

    private fun init() {
        val networkInterfaces = NetworkInterface.getNetworkInterfaces()
        while (networkInterfaces.hasMoreElements()) {
            val networkInterface = networkInterfaces.nextElement()
            if (networkInterface.isLoopback || !networkInterface.isUp) {
                continue
            }
            networkInterface.interfaceAddresses.forEach { interfaceAddr ->
                val isIPv4 = interfaceAddr.address is Inet4Address
                if (isIPv4 && interfaceAddr.address == deviceAddr) {
                    val address =
                        interfaceAddr.address.address.map(Byte::toPositiveInt).toIntArray()
                    val maskLength = interfaceAddr.networkPrefixLength.toInt()
                    val mask = genMask(maskLength)

                    val netAddr = address.zip(mask).map { (a, m) -> a and m }.toIntArray()

                    networkAddress = netAddr.toInetAddress()
                    hostsQty = (2.0.pow(32.0 - maskLength) - 1).toInt()
                    broadcast = interfaceAddr.broadcast
                }
            }
        }
    }

    private fun genMask(len: Int): IntArray {
        val maskStr = "1".repeat(len).padEnd(32, '0')
        return maskStr.chunked(8)
            .map { s -> s.toInt(2) }
            .toIntArray()
    }
}