package josercl.vlcremote.net

import com.squareup.moshi.Moshi
import io.ktor.client.HttpClient
import io.ktor.client.engine.android.Android
import io.ktor.client.features.defaultRequest
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.features.logging.LogLevel
import io.ktor.client.features.logging.Logging
import io.ktor.client.request.accept
import io.ktor.client.request.header
import io.ktor.client.request.parameter
import io.ktor.client.request.request
import io.ktor.http.ContentType
import io.ktor.http.HttpHeaders
import io.ktor.http.HttpMethod
import io.ktor.http.contentType
import josercl.vlcremote.extensions.encodePassword
import josercl.vlcremote.model.enum.VlcCommand
import josercl.vlcremote.model.net.BrowseResponse
import josercl.vlcremote.model.net.PlaylistNode
import josercl.vlcremote.model.net.StatusResponse

class VlcClient(
    private val moshi: Moshi,
    private val host: String,
    private val port: String,
    private val password: String,
) {
    private val client = HttpClient(Android) {
        install(Logging) {
            this.level = LogLevel.BODY
        }

        install(JsonFeature) {
            acceptContentTypes = acceptContentTypes + ContentType("text", "plain")
            serializer = MoshiSerializer(moshi)
        }

        defaultRequest {
            method = HttpMethod.Get

            url {
                host = this@VlcClient.host
                port = this@VlcClient.port.toInt()
            }

            header(HttpHeaders.Authorization, "Basic ${password.encodePassword()}")

            contentType(ContentType.Application.Json)
            accept(ContentType.Application.Json)
        }
    }

    suspend fun status(
        command: VlcCommand? = null,
        args: Map<String, String> = mapOf()
    ): StatusResponse = client.request("requests/status.json") {
        command?.run {
            parameter("command", toString())
        }
        args.forEach {
            parameter(it.key, it.value)
        }
    }

    suspend fun browse(dir: String): BrowseResponse = client.request("requests/browse.json") {
        parameter("uri", dir)
    }

    suspend fun getPlaylist(): PlaylistNode = client.request("requests/playlist.json")
}