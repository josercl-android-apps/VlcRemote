package josercl.vlcremote.net

import android.content.SharedPreferences
import com.squareup.moshi.Moshi
import josercl.vlcremote.extensions.getHost
import josercl.vlcremote.extensions.getPassword
import josercl.vlcremote.extensions.getPort

class VlcClientManager(
    private val moshi: Moshi,
    private val preferences: SharedPreferences
) {
    @Volatile private var vlcClient: VlcClient? = null

    fun getClient(): VlcClient {
        return vlcClient ?: synchronized(this) {
            vlcClient ?: VlcClient(moshi, preferences.getHost(), preferences.getPort(), preferences.getPassword())
        }
    }

    fun resetClient() {
        vlcClient = null
    }
}