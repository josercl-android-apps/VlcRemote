package josercl.vlcremote.db.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import josercl.vlcremote.db.entity.Favorite
import kotlinx.coroutines.flow.Flow

@Dao
interface FavoriteDao {
  @Query("SELECT * FROM favorite")
  fun getAll(): Flow<List<Favorite>>

  @Insert
  suspend fun save(favorite: Favorite)

  @Update
  suspend fun update(favorite: Favorite)

  @Delete
  suspend fun delete(favorite: Favorite)
}