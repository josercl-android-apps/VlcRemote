package josercl.vlcremote.db.repository

import josercl.vlcremote.db.dao.FavoriteDao
import josercl.vlcremote.model.mapper.EntityDomainMapper
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.mapLatest
import josercl.vlcremote.db.entity.Favorite as DbEntityFavorite
import josercl.vlcremote.model.Favorite as DomainFavorite

@ExperimentalCoroutinesApi
class FavoriteRepository(
    private val dao: FavoriteDao,
    private val mapper: EntityDomainMapper<DbEntityFavorite, DomainFavorite>
) {

    fun getAll(): Flow<List<DomainFavorite>> {
        return dao.getAll()
            .mapLatest { it.map(mapper::toDomain) }
            .distinctUntilChanged()
    }

    suspend fun save(favorite: DomainFavorite) {
        dao.save(mapper.toDatabase(favorite))
    }

    suspend fun update(favorite: DomainFavorite) {
        dao.update(mapper.toDatabase(favorite))
    }

    suspend fun delete(favorite: DomainFavorite) {
        dao.delete(mapper.toDatabase(favorite))
    }

}