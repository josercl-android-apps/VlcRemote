package josercl.vlcremote.db

import androidx.room.Database
import androidx.room.RoomDatabase
import josercl.vlcremote.db.dao.FavoriteDao
import josercl.vlcremote.db.entity.Favorite

@Database(entities = [Favorite::class], version = 2, exportSchema = false)
abstract class VlcRemoteDatabase: RoomDatabase() {
  abstract fun favoriteDao(): FavoriteDao
}