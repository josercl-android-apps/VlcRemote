package josercl.vlcremote.compose

sealed class NavEvent {
    object GoBack: NavEvent()
    data class GoTo(val destination: String, val navArgs: Map<String, Any> = emptyMap()): NavEvent()
    data class PopAndGoTo(
        val destination: String,
        val popUpToDestination: String,
        val inclusive: Boolean
    ): NavEvent()
}