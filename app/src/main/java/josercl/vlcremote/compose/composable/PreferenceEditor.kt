package josercl.vlcremote.compose.composable

import android.content.SharedPreferences
import android.content.res.Configuration.UI_MODE_NIGHT_YES
import androidx.annotation.DrawableRes
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.LocalContentColor
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.core.content.edit
import androidx.preference.PreferenceManager
import josercl.vlcremote.PREF_HOST
import josercl.vlcremote.compose.R
import josercl.vlcremote.compose.ui.theme.VlcRemoteTheme
import josercl.vlcremote.extensions.hidePassword

@ExperimentalMaterial3Api
@Composable
fun PreferenceEditor(
    modifier: Modifier = Modifier,
    @DrawableRes icon: Int = 0,
    title: String,
    prefKey: String,
    prefDefaultValue: String = "",
    password: Boolean = false,
    numeric: Boolean = false,
    preferences: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(LocalContext.current)
) {
    var showDialog by remember { mutableStateOf(false) }

    val oldValue: String = remember { preferences.getString(prefKey, prefDefaultValue)!! }

    val (value, setValue) = remember {
        mutableStateOf(preferences.getString(prefKey, prefDefaultValue)!!)
    }

    val (summary, setSummary) = remember {
        var prefVal = preferences.getString(prefKey, prefDefaultValue)!!
        if (password) {
            prefVal = prefVal.hidePassword()
        }
        mutableStateOf(prefVal)
    }

    var passwordVisible by remember { mutableStateOf(!password) }

    fun save() {
        preferences.edit(true) {
            putString(prefKey, value)
        }
        if (value != oldValue) {
            val newSummary = when {
                password -> value.hidePassword()
                else -> value
            }
            setSummary(newSummary)
        }
        showDialog = false
    }

    if (showDialog) {
        AlertDialog(
            title = { Text(text = title) },
            onDismissRequest = { showDialog = false },
            confirmButton = {
                TextButton(
                    onClick = { save() }
                ) {
                    Text(text = stringResource(id = R.string.ok))
                }
            },
            dismissButton = {
                TextButton(
                    onClick = {
                        showDialog = false
                    }
                ) {
                    Text(text = stringResource(id = R.string.cancel))
                }
            },
            text = {
                OutlinedTextField(
                    value = value,
                    onValueChange = {
                        setValue(when {
                            numeric -> it.trim().filter(Char::isDigit)
                            else -> it.trim()
                        })
                    },
                    visualTransformation = when {
                        passwordVisible -> VisualTransformation.None
                        else -> PasswordVisualTransformation()
                    },
                    maxLines = 1,
                    singleLine = true,
                    keyboardActions = KeyboardActions(
                        onDone = {
                            save()
                        }
                    ),
                    keyboardOptions = KeyboardOptions(
                        imeAction = ImeAction.Done,
                        autoCorrect = false,
                        keyboardType = when {
                            password -> KeyboardType.Password
                            numeric -> KeyboardType.Number
                            else -> KeyboardType.Text
                        }
                    ),
                    trailingIcon = when {
                        password -> {
                            {
                                IconButton(onClick = { passwordVisible = !passwordVisible }) {
                                    Icon(
                                        painterResource(id = when {
                                            passwordVisible -> R.drawable.ic_visibility
                                            else -> R.drawable.ic_visibility_off
                                        }),
                                        contentDescription = ""
                                    )
                                }
                            }
                        }
                        else -> null
                    }
                )
            }
        )
    }

    Row(
        modifier = modifier
            .fillMaxWidth()
            .height(64.dp)
            .clickable { showDialog = true }
            .padding(horizontal = 16.dp),
        verticalAlignment = Alignment.CenterVertically
    ) {
        if (icon != 0) {
            Icon(
                painter = painterResource(id = icon),
                contentDescription = ""
            )
        } else {
            Spacer(modifier = Modifier.width(24.dp))
        }
        Spacer(modifier = Modifier.width(32.dp))
        Column(
            modifier = Modifier
                .weight(1f)
                .fillMaxHeight(),
            verticalArrangement = Arrangement.Center
        ) {
            Text(
                text = title,
                modifier = Modifier.fillMaxWidth(),
                maxLines = 1,
                overflow = TextOverflow.Ellipsis,
                style = MaterialTheme.typography.bodyLarge
            )
            if (summary.isNotEmpty()) {
                Text(
                    text = summary,
                    modifier = Modifier.fillMaxWidth(),
                    maxLines = 1,
                    overflow = TextOverflow.Ellipsis,
                    style = MaterialTheme.typography.bodyMedium,
                    color = LocalContentColor.current.copy(alpha = .6f)
                )
            }
        }
    }
}

@ExperimentalMaterial3Api
@Preview
@Preview(uiMode = UI_MODE_NIGHT_YES)
@Composable
fun PreferenceEditorPreview() {
    VlcRemoteTheme {
        Surface {
            PreferenceEditor(
                icon = R.drawable.ic_wrench,
                title = stringResource(id = R.string.pref_server_title),
                prefKey = PREF_HOST,
                prefDefaultValue = stringResource(id = R.string.pref_server_default)
            )
        }
    }
}