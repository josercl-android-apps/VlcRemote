package josercl.vlcremote.compose.composable

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.ArrowBack
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import josercl.vlcremote.compose.ui.theme.VlcRemoteTheme

@Composable
fun NavBackButton(
    modifier: Modifier = Modifier,
    onClick: () -> Unit = {}
) {
    IconButton(
        modifier = modifier,
        onClick = onClick
    ) {
        Icon(
            imageVector = Icons.Rounded.ArrowBack,
            contentDescription = ""
        )
    }
}

@Preview
@Composable
fun NavBackButtonPreview() {
    VlcRemoteTheme {
        NavBackButton()
    }
}