package josercl.vlcremote.compose.composable

import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import josercl.vlcremote.compose.ui.theme.VlcRemoteTheme

@ExperimentalMaterial3Api
@ExperimentalLayoutApi
@Composable
fun VlcRemoteTopBar(
    modifier: Modifier = Modifier,
    title: String,
    onNavigationIconClick: () -> Unit = {},
) {
    CenterAlignedTopAppBar(
        modifier = modifier,
        navigationIcon = {
            NavBackButton(
                onClick = onNavigationIconClick
            )
        },
        title = {
            Text(
                text = title,
            )
        },
    )
}

@ExperimentalMaterial3Api
@ExperimentalLayoutApi
@Preview
@Composable
fun VlcRemoteTopBarPreview() {
    VlcRemoteTheme {
        VlcRemoteTopBar(
            title = "Title Here",
            onNavigationIconClick = {}
        )
    }
}