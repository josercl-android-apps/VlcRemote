package josercl.vlcremote.compose.composable

import android.content.res.Configuration.UI_MODE_NIGHT_YES
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Icon
import androidx.compose.material3.LocalContentColor
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.unit.dp
import josercl.vlcremote.compose.ui.preview.BooleanPreviewParameterProvider
import josercl.vlcremote.compose.ui.theme.VlcRemoteTheme
import josercl.vlcremote.extensions.formatSeconds
import josercl.vlcremote.extensions.getFileIcon
import josercl.vlcremote.model.net.PlaylistNode

@Composable
fun PlaylistItem(
    item: PlaylistNode,
    active: Boolean,
    onClick: () -> Unit,
) {
    val bgColor = when {
        active -> MaterialTheme.colorScheme.tertiaryContainer
        else -> Color.Transparent
    }
    val contentColor = when {
        active -> MaterialTheme.colorScheme.onTertiaryContainer
        else -> LocalContentColor.current
    }
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .background(color = bgColor)
            .clickable { onClick() }
            .padding(all = 16.dp),
        verticalAlignment = Alignment.CenterVertically,
    ) {
        Icon(
            painter = painterResource(id = item.uri.getFileIcon()),
            contentDescription = "",
            tint = contentColor
        )
        Spacer(modifier = Modifier.width(16.dp))
        Column(modifier = Modifier.weight(1f)) {
            Text(
                text = item.name,
                maxLines = 1,
                overflow = TextOverflow.Ellipsis,
                style = MaterialTheme.typography.bodyMedium,
                color = contentColor
            )
            Text(
                text = (item.duration?.toLong() ?: 0L).formatSeconds(),
                maxLines = 1,
                overflow = TextOverflow.Ellipsis,
                style = MaterialTheme.typography.bodySmall,
                color = contentColor
            )
        }
    }
}

@Preview
@Preview(uiMode = UI_MODE_NIGHT_YES)
@Composable
fun PlaylistItemPreview(
    @PreviewParameter(BooleanPreviewParameterProvider::class) active: Boolean
) {
    VlcRemoteTheme {
        Surface {
            PlaylistItem(
                item = PlaylistNode(
                    0,
                    emptyList(),
                    "Playlist Item",
                    "",
                    "",
                    null,
                    3600,
                    "playlist_item.mp4"
                ),
                active = active,
                onClick = {}
            )
        }
    }
}