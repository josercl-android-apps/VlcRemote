package josercl.vlcremote.compose

import androidx.annotation.DrawableRes
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import josercl.vlcremote.compose.ui.theme.VlcRemoteTheme

@Composable
fun RoundedButton(
    modifier: Modifier = Modifier,
    onClick: () -> Unit,
    @DrawableRes icon: Int,
    contentColor: Color = MaterialTheme.colorScheme.primary,
    borderColor: Color = contentColor,
    containerColor: Color = Color.Transparent,
    iconSize: Dp = 24.dp,
    contentPadding: Dp = 0.dp
) {
    IconButton(
        onClick = onClick,
        /*colors = ButtonDefaults.outlinedButtonColors(
            contentColor = contentColor
        ),
        modifier = Modifier.defaultMinSize(minWidth = 0.dp, minHeight = 0.dp),
        contentPadding = PaddingValues(vertical = 12.dp, horizontal = 0.dp),
        shape = CircleShape,
        border = BorderStroke(
            width = 2.dp,
            color = borderColor
        )*/
        modifier = modifier.border(
            width = 2.dp,
            color = borderColor,
            shape = CircleShape
        ).background(
            color = containerColor,
            shape = CircleShape
        ).padding(all = contentPadding)
    ) {
        Icon(
            modifier = Modifier.size(iconSize),
            painter = painterResource(id = icon),
            contentDescription = "",
            tint = contentColor
        )
    }
}

@Preview
@Composable
fun RoundedButtonPreview() {
    VlcRemoteTheme {
        RoundedButton(
            onClick = { /*TODO*/ },
            icon = R.drawable.ic_forward_30
        )
    }
}