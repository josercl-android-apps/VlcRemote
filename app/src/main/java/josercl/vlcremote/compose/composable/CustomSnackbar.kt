package josercl.vlcremote.compose.composable

import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Snackbar
import androidx.compose.material3.SnackbarData
import androidx.compose.material3.SnackbarDuration
import androidx.compose.material3.SnackbarVisuals
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import josercl.vlcremote.compose.ui.theme.VlcRemoteTheme

@Composable
fun CustomSnackbar(snackbarData: SnackbarData) {
    val actionLabel = snackbarData.visuals.actionLabel
    val actionComposable: (@Composable () -> Unit)? = if (actionLabel != null) {
        @Composable {
            TextButton(
                colors = ButtonDefaults.textButtonColors(
                    contentColor = MaterialTheme.colorScheme.inversePrimary
                ),
                onClick = { snackbarData.performAction() },
                content = { Text(actionLabel) }
            )
        }
    } else {
        null
    }
    val dismissActionComposable: (@Composable () -> Unit)? =
        if (snackbarData.visuals.withDismissAction) {
            @Composable {
                IconButton(
                    onClick = { snackbarData.dismiss() },
                    content = {
                        Icon(
                            Icons.Filled.Close,
                            contentDescription = null,
                        )
                    }
                )
            }
        } else {
            null
        }


    Snackbar(
        modifier = Modifier
            .padding(bottom = 12.dp),
        action = actionComposable,
        dismissAction = dismissActionComposable,
        actionOnNewLine = false,
        shape = RectangleShape,
        containerColor = MaterialTheme.colorScheme.inverseSurface,
        contentColor = MaterialTheme.colorScheme.inverseOnSurface,
        content = { Text(snackbarData.visuals.message) }
    )
}

@Preview
@Composable
fun CustomSnackbarPreview() {
    VlcRemoteTheme {
        Snackbar(snackbarData = object: SnackbarData {
            override val visuals: SnackbarVisuals
                get() = object: SnackbarVisuals {
                    override val actionLabel: String?
                        get() = null
                    override val duration: SnackbarDuration
                        get() = SnackbarDuration.Indefinite
                    override val message: String
                        get() = "Message"
                    override val withDismissAction: Boolean
                        get() = true
                }

            override fun dismiss() {}

            override fun performAction() {}
        })
    }
}