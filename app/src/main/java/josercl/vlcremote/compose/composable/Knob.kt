package josercl.vlcremote.compose.composable

import android.view.MotionEvent
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.size
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.StrokeCap
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.graphics.drawscope.rotate
import androidx.compose.ui.input.pointer.pointerInteropFilter
import androidx.compose.ui.layout.boundsInWindow
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
// import josercl.vlcremote.extensions.darken
import josercl.vlcremote.compose.ui.theme.VlcRemoteTheme
import kotlin.math.PI
import kotlin.math.atan2

@ExperimentalComposeUiApi
@Composable
fun Knob(
    modifier: Modifier = Modifier,
    size: Dp = 112.dp,
    limitingAngle: Float = 45f,
    range: ClosedFloatingPointRange<Float> = 0f..1f,
    value: Float = 0f,
    progressColor: Color = MaterialTheme.colorScheme.secondary,
    circleColor: Color = MaterialTheme.colorScheme.onBackground,
    onValueChange: (Float) -> Unit,
    onStartDragging: () -> Unit = {},
    onStopDragging: () -> Unit = {},
) {
    val maxAngle = 360f - limitingAngle * 2f
    val progress = value * maxAngle / range.endInclusive

    val progressWidth = 16.dp

    var touchX by remember { mutableStateOf(0f) }
    var touchY by remember { mutableStateOf(0f) }
    var centerX by remember { mutableStateOf(0f) }
    var centerY by remember { mutableStateOf(0f) }

    val colors = listOf(
        progressColor,
        progressColor
    )

    Box(
        modifier = Modifier
            .size(size)
            .then(modifier)
    ) {
        Canvas(
            modifier = Modifier
                .fillMaxSize()
                .onGloballyPositioned {
                    val windowBounds = it.boundsInWindow()
                    centerX = windowBounds.size.width / 2f
                    centerY = windowBounds.size.height / 2f
                }
                .pointerInteropFilter { event ->
                    touchX = event.x
                    touchY = event.y

                    val angle = -atan2(
                        centerX - touchX,
                        centerY - touchY
                    ) * (180f / PI).toFloat() + 180f - limitingAngle

                    when (event.action) {
                        MotionEvent.ACTION_DOWN -> {
                            onStartDragging()
                            true
                        }
                        MotionEvent.ACTION_UP -> {
                            onStopDragging()
                            true
                        }
                        MotionEvent.ACTION_MOVE -> {
                            if (angle in 0f..maxAngle) {
                                onValueChange(range.endInclusive * angle / maxAngle)
                                true
                            } else false
                        }
                        else -> false
                    }
                }
        ) {
            val circleRadius = (this.size.width / 2) - progressWidth.toPx() - 12.dp.toPx()

            rotate(degrees = limitingAngle + progress) {
                drawLine(
                    color = circleColor,
                    start = center + Offset(0f, circleRadius - 8.dp.toPx()),
                    end = center + Offset(0f, circleRadius),
                    strokeWidth = 4.dp.toPx(),
                    cap = StrokeCap.Round,
                )
            }

            rotate(degrees = limitingAngle) {
                drawArc(
                    color = progressColor.copy(alpha = .15f),
                    startAngle = limitingAngle * 2,
                    sweepAngle = 360 - limitingAngle * 2,
                    useCenter = false,
                    style = Stroke(width = progressWidth.toPx(), cap = StrokeCap.Round),
                    size = this.size.copy(
                        this.size.width - progressWidth.toPx(),
                        this.size.height - progressWidth.toPx(),
                    ),
                    topLeft = Offset(
                        (progressWidth / 2).toPx(),
                        (progressWidth / 2).toPx()
                    )
                )
                drawArc(
                    color = progressColor,
                    startAngle = limitingAngle * 2,
                    sweepAngle = progress,
                    useCenter = false,
                    style = Stroke(width = progressWidth.toPx(), cap = StrokeCap.Round),
                    size = this.size.copy(
                        this.size.width - progressWidth.toPx(),
                        this.size.height - progressWidth.toPx(),
                    ),
                    topLeft = Offset(
                        (progressWidth / 2).toPx(),
                        (progressWidth / 2).toPx()
                    )
                )
            }

            /*drawCircle(
                color = circleColor,
                radius = circleRadius,
                style = Stroke(width = 1.dp.toPx())
            )*/
        }
    }
}


@ExperimentalComposeUiApi
@Preview
//@Preview(uiMode = UI_MODE_NIGHT_YES)
@Composable
fun KnobPreview() {
    VlcRemoteTheme {
        Knob(
            range = 0f..100f,
            value = 55f,
            onValueChange = {}
        )
    }
}