package josercl.vlcremote

const val SERVICE_NOTIFICATION_ID = 15372914
const val SERVICE_EXTRA_STATUS = "EXTRA_STATUS"

const val UPDATE_STATUS_ACTION = "josercl.vlcremote.compose.`UPDATE_STATUS_ACTION$1`"

const val REQUEST_PERMISSION_WIFI_STATE = 1000

const val PREF_FIRST_RUN = "pref_first_run"
const val PREF_HOST = "vlc_server"
const val PREF_PASS = "vlc_pass"
const val PREF_PORT = "vlc_port"
const val PREF_M3 = "material3"
const val PREF_THEME = "theme"

object Extensions{
    val VIDEO = arrayOf("mkv", "avi", "mp4", "mpg", "mpeg")
    val IMAGE = arrayOf("jpg", "jpeg", "gif", "png", "webp")
    val TEXT = arrayOf("txt", "nfo", "srt")
    val AUDIO = arrayOf("mp3", "wav", "m4a")
}

object Routes {
    const val HOME = "home"
    const val SETTINGS = "settings"
    const val PLAYLIST = "playlist"
    const val FAVORITES = "favorites"
    const val AUTO_CONF = "autoconfigure"
    const val BROWSE = "browse?uri={uri}"
    const val TUTORIAL = "tutorial"
    const val EQUALIZER = "equalizer"
}