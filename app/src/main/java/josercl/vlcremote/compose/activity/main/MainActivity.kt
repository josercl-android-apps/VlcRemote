package josercl.vlcremote.compose.activity.main

import android.content.SharedPreferences
import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.foundation.layout.navigationBarsPadding
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.navigation.NavHostController
import androidx.navigation.NavOptions
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.rememberNavController
import com.squareup.moshi.Moshi
import dagger.hilt.android.AndroidEntryPoint
import josercl.vlcremote.Routes
import josercl.vlcremote.compose.LocalMoshi
import josercl.vlcremote.compose.LocalPreferences
import josercl.vlcremote.compose.NavEvent
import josercl.vlcremote.compose.ui.theme.VlcRemoteTheme
import josercl.vlcremote.extensions.isFirstRun
import josercl.vlcremote.extensions.setIsFirstRun
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

@ExperimentalLayoutApi
@ExperimentalFoundationApi
@ExperimentalCoroutinesApi
@ExperimentalComposeUiApi
@ExperimentalMaterial3Api
@ExperimentalAnimationApi
@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private val viewModel: MainViewModel by viewModels()

    @Inject lateinit var preferences: SharedPreferences

    @Inject lateinit var moshi: Moshi

    private lateinit var navController: NavHostController

    override fun onCreate(savedInstanceState: Bundle?) {
        installSplashScreen()

        super.onCreate(savedInstanceState)

        setContent {
            navController = rememberNavController()

            LaunchedEffect(key1 = Unit) {
                if (preferences.isFirstRun()) {
                    navEventProcessor(NavEvent.GoTo(Routes.SETTINGS))
                    navEventProcessor(NavEvent.GoTo(Routes.TUTORIAL))
                    preferences.setIsFirstRun(false)
                }
            }

            val dynamicColors by viewModel.dynamicColors.collectAsStateWithLifecycle()

            val defaultNightMode by viewModel.theme.collectAsStateWithLifecycle()
            val darkThemeOn =
                (defaultNightMode == AppCompatDelegate.MODE_NIGHT_YES) ||
                        (defaultNightMode in arrayOf(
                            AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM,
                            AppCompatDelegate.MODE_NIGHT_AUTO_BATTERY
                        ) && isSystemInDarkTheme())

            VlcRemoteTheme(
                darkTheme = darkThemeOn,
                dynamicColor = dynamicColors
            ) {
                CompositionLocalProvider(
                    LocalPreferences provides preferences,
                    LocalMoshi provides moshi,
                ) {
                    NavHost(
                        modifier = Modifier.navigationBarsPadding(),
                        navController = navController,
                        startDestination = Routes.HOME
                    ) {
                        mainScreen(
                            moshi = moshi,
                            navProcessor = this@MainActivity::navEventProcessor
                        )

                        favoritesScreen(
                            onSettingsChanged = { },
                            navProcessor = this@MainActivity::navEventProcessor
                        )

                        playlistScreen(
                            viewModel = viewModel,
                            navProcessor = this@MainActivity::navEventProcessor
                        )

                        settingsScreen(
                            navProcessor = this@MainActivity::navEventProcessor
                        )

                        equalizerScreen(
                            navProcessor = this@MainActivity::navEventProcessor
                        )

                        browseScreen(
                            onUpdatePlaylist = { viewModel.setRefreshPlaylist(true) },
                            navProcessor = this@MainActivity::navEventProcessor
                        )

                        autoconfigurationScreen(
                            onSettingsChanged = {},
                            navProcessor = this@MainActivity::navEventProcessor
                        )

                        tutorialScreen(
                            navProcessor = this@MainActivity::navEventProcessor
                        )
                    }
                }
            }
        }
    }

    private fun navEventProcessor(event: NavEvent) {
        when (event) {
            NavEvent.GoBack -> navController.popBackStack()
            is NavEvent.GoTo -> navController.navigate(route = event.destination)
            is NavEvent.PopAndGoTo -> {
                navController.navigate(
                    event.destination,
                    NavOptions.Builder()
                        .setPopUpTo(event.popUpToDestination, inclusive = event.inclusive)
                        .build()
                )
            }
        }
    }
}