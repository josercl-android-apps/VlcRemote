package josercl.vlcremote.compose.activity.main

import androidx.compose.animation.EnterTransition
import androidx.compose.animation.ExitTransition
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavType
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import com.squareup.moshi.Moshi
import josercl.vlcremote.Routes
import josercl.vlcremote.compose.NavEvent
import josercl.vlcremote.compose.features.autoconfigure.AutoConfigureScreen
import josercl.vlcremote.compose.features.browse.BrowseScreen
import josercl.vlcremote.compose.features.equalizer.EqualizerScreen
import josercl.vlcremote.compose.features.favorites.FavoriteScreen
import josercl.vlcremote.compose.features.main.MainScreen
import josercl.vlcremote.compose.features.playlist.PlaylistScreen
import josercl.vlcremote.compose.features.settings.SettingsScreen
import josercl.vlcremote.compose.features.tutorial.TutorialScreen
import josercl.vlcremote.extensions.slideFromBottom
import josercl.vlcremote.extensions.slideFromLeft
import josercl.vlcremote.extensions.slideFromRight
import josercl.vlcremote.extensions.slideFromTop
import josercl.vlcremote.extensions.slideToBottom
import josercl.vlcremote.extensions.slideToLeft
import josercl.vlcremote.extensions.slideToRight
import josercl.vlcremote.extensions.slideToTop
import josercl.vlcremote.model.net.Equalizer
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalLayoutApi
@ExperimentalCoroutinesApi
@ExperimentalComposeUiApi
@ExperimentalMaterial3Api
@ExperimentalAnimationApi
fun NavGraphBuilder.mainScreen(moshi: Moshi, navProcessor: (NavEvent) -> Unit) {
    composable(
        Routes.HOME,
        popEnterTransition = {
            when (this.initialState.destination.route) {
                Routes.FAVORITES -> slideFromRight()
                Routes.PLAYLIST -> slideFromTop()
                Routes.SETTINGS -> slideFromLeft()
                else -> EnterTransition.None
            }
        },
        exitTransition = {
            when (this.targetState.destination.route) {
                Routes.FAVORITES -> slideToRight()
                Routes.PLAYLIST -> slideToTop()
                Routes.SETTINGS -> slideToLeft()
                else -> ExitTransition.None
            }
        }
    ) {
        MainScreen(
            toJson = { moshi.adapter(Equalizer::class.java).toJson(it as Equalizer) },
            onNavEvent = navProcessor
        )
    }
}

@ExperimentalLayoutApi
@ExperimentalCoroutinesApi
@ExperimentalMaterial3Api
@ExperimentalAnimationApi
fun NavGraphBuilder.favoritesScreen(onSettingsChanged: () -> Unit, navProcessor: (NavEvent) -> Unit) {
    composable(
        Routes.FAVORITES,
        enterTransition = { slideFromLeft() },
        exitTransition = { slideToLeft() },
    ) {
        FavoriteScreen(
            onNavEvent = navProcessor,
            onSettingsChanged = onSettingsChanged
        )
    }
}

@ExperimentalFoundationApi
@ExperimentalLayoutApi
@ExperimentalMaterial3Api
@ExperimentalAnimationApi
fun NavGraphBuilder.playlistScreen(viewModel: MainViewModel, navProcessor: (NavEvent) -> Unit) {
    composable(
        Routes.PLAYLIST,
        enterTransition = { slideFromBottom() },
        exitTransition = { slideToBottom() }
    ) {
        PlaylistScreen(
            mainViewModel = viewModel,
            onNavEvent = navProcessor
        )
    }
}

@ExperimentalLayoutApi
@ExperimentalMaterial3Api
@ExperimentalAnimationApi
fun NavGraphBuilder.settingsScreen(
    navProcessor: (NavEvent) -> Unit
) {
    composable(
        Routes.SETTINGS,
        enterTransition = { slideFromRight() },
        popEnterTransition = { fadeIn() },
        popExitTransition = { slideToRight() }
    ) {
        SettingsScreen(
            onNavEvent = navProcessor,
        )
    }
}

@ExperimentalLayoutApi
@ExperimentalMaterial3Api
@ExperimentalFoundationApi
@ExperimentalAnimationApi
fun NavGraphBuilder.equalizerScreen(
    navProcessor: (NavEvent) -> Unit
) {
    composable(
        route = "${Routes.EQUALIZER}/{equalizer}/{equalizerEnabled}",
        arguments = listOf(
            navArgument("equalizer") {
                type = NavType.StringType
                nullable = false
            },
            navArgument("equalizerEnabled") {
                type = NavType.IntType
                nullable = false
            }
        ),
        enterTransition = { fadeIn() },
        exitTransition = { fadeOut() }
    ) {
        EqualizerScreen(onNavEvent = navProcessor)
    }
}

@ExperimentalLayoutApi
@ExperimentalMaterial3Api
@ExperimentalAnimationApi
fun NavGraphBuilder.browseScreen(
    onUpdatePlaylist: () -> Unit,
    navProcessor: (NavEvent) -> Unit
) {
    composable(
        Routes.BROWSE,
        arguments = listOf(
            navArgument("uri") {
                type = NavType.StringType
                defaultValue = "file:///"
            }
        )
    ) {
        BrowseScreen(
            onNavEvent = navProcessor,
            onUpdatePlaylist = onUpdatePlaylist
        )
    }
}

@ExperimentalLayoutApi
@ExperimentalMaterial3Api
@ExperimentalAnimationApi
fun NavGraphBuilder.autoconfigurationScreen(
    onSettingsChanged: (Boolean) -> Unit,
    navProcessor: (NavEvent) -> Unit
) {
    composable(Routes.AUTO_CONF) {
        AutoConfigureScreen(
            onNavEvent = navProcessor,
            onSettingsChanged = onSettingsChanged
        )
    }
}

@OptIn(ExperimentalFoundationApi::class)
@ExperimentalMaterial3Api
@ExperimentalAnimationApi
fun NavGraphBuilder.tutorialScreen(navProcessor: (NavEvent) -> Unit) {
    composable(Routes.TUTORIAL) {
        TutorialScreen(onNavEvent = navProcessor)
    }
}
