package josercl.vlcremote.compose.activity.main

import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatDelegate
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import josercl.androidutils.flow.preferenceFlow
import josercl.vlcremote.PREF_M3
import josercl.vlcremote.PREF_THEME
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

data class MainState(
    val settingsChanged: Boolean = false,
    val refreshPlaylist: Boolean = false,
)

@HiltViewModel
class MainViewModel @Inject constructor(
    preferences: SharedPreferences,
) : ViewModel() {

    private val _state = MutableStateFlow(MainState())
    val state = _state.asStateFlow()

    private val _dynamicColors = preferenceFlow(preferences, PREF_M3, false)
    val dynamicColors = _dynamicColors.stateIn(viewModelScope, SharingStarted.WhileSubscribed(5000), false)

    private val _theme = preferenceFlow(preferences, PREF_THEME, AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM)
    val theme = _theme.stateIn(viewModelScope, SharingStarted.WhileSubscribed(5000), AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM)

    fun setRefreshPlaylist(refresh: Boolean) = _state.update { it.copy(refreshPlaylist = refresh) }
}