package josercl.vlcremote.compose.features.browse

import android.content.res.Configuration
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import josercl.vlcremote.compose.R
import josercl.vlcremote.compose.ui.theme.VlcRemoteTheme

@Composable
fun FolderActions(
    fileName: String,
    onEnqueue: () -> Unit,
    onDismiss: () -> Unit
) {
    Column(
        modifier = Modifier.fillMaxWidth()
    ) {
        Text(
            text = fileName,
            maxLines = 1,
            overflow = TextOverflow.Ellipsis,
            style = MaterialTheme.typography.bodyMedium,
            modifier = Modifier
                .fillMaxWidth()
                .padding(all = 16.dp),
            fontWeight = FontWeight.Bold,
        )
        HorizontalDivider(
            thickness = 1.dp,
            color = MaterialTheme.colorScheme.primary
        )
        FileActionButton(
            icon = R.drawable.ic_playlist_add,
            text = R.string.folder_action_enqueue,
            onClick = onEnqueue,
            modifier = Modifier.fillMaxWidth()
        )
        FileActionButton(
            icon = R.drawable.ic_close,
            text = R.string.cancel,
            onClick = onDismiss,
            contentColor = MaterialTheme.colorScheme.error,
            modifier = Modifier.fillMaxWidth()
        )
    }
}

@Preview(group = "Light")
@Preview(uiMode = Configuration.UI_MODE_NIGHT_YES, group = "Dark")
@Composable
fun FolderActionsPreview() {
    VlcRemoteTheme {
        Surface {
            FolderActions(
                "Folder name",
                onEnqueue = {},
                onDismiss = {}
            )
        }
    }
}