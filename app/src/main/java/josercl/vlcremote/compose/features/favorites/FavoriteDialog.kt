package josercl.vlcremote.compose.features.favorites

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import josercl.vlcremote.compose.R
import josercl.vlcremote.compose.ui.theme.VlcRemoteTheme
import josercl.vlcremote.model.Favorite

@ExperimentalMaterial3Api
@Composable
fun FavoriteDialog(
    favorite: Favorite,
    onAliasChanged: (String) -> Unit,
    onServerChanged: (String) -> Unit,
    onPortChanged: (String) -> Unit,
    onPasswordChanged: (String) -> Unit,
    onConfirm: () -> Unit,
    onDismissRequest: () -> Unit,
    onDismiss: () -> Unit = {},
) {
    AlertDialog(
        onDismissRequest = onDismissRequest,
        confirmButton = {
            TextButton(
                onClick = {
                    onConfirm()
                    onDismissRequest()
                }
            ) {
                Text(text = stringResource(id = R.string.ok))
            }
        },
        dismissButton = {
            TextButton(
                onClick = {
                    onDismiss()
                    onDismissRequest()
                }
            ) {
                Text(text = stringResource(id = R.string.cancel))
            }
        },
        title = {
            Text(
                text = stringResource(
                    id = when (favorite.isNew) {
                        true -> R.string.favorite_dialog_new_title
                        else -> R.string.favorite_dialog_edit_title
                    }
                )
            )
        },
        text = {
            FavoriteDialogBody(
                favorite,
                onAliasChanged,
                onServerChanged,
                onPortChanged,
                onPasswordChanged
            )
        }
    )

}

@ExperimentalMaterial3Api
@Composable
private fun FavoriteDialogBody(
    favorite: Favorite,
    onAliasChanged: (String) -> Unit,
    onServerChanged: (String) -> Unit,
    onPortChanged: (String) -> Unit,
    onPasswordChanged: (String) -> Unit,
) {
    var passwordVisible by remember { mutableStateOf(false) }

    var alias by remember { mutableStateOf(favorite.alias) }
    var server by remember { mutableStateOf(favorite.server) }
    var port by remember { mutableStateOf(favorite.port.toString()) }
    var pass by remember { mutableStateOf(favorite.pass) }

    Column(modifier = Modifier.fillMaxWidth()) {
        OutlinedTextField(
            label = {
                Text(text = stringResource(id = R.string.favorite_dialog_alias_hint))
            },
            singleLine = true,
            maxLines = 1,
            modifier = Modifier.fillMaxWidth(),
            value = alias,
            onValueChange = {
                alias = it
                onAliasChanged(it)
            }
        )
        OutlinedTextField(
            label = {
                Text(text = stringResource(id = R.string.favorite_dialog_server_hint))
            },
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 8.dp),
            value = server,
            onValueChange = {
                server = it
                onServerChanged(it)
            }
        )
        OutlinedTextField(
            label = {
                Text(text = stringResource(id = R.string.favorite_dialog_port_hint))
            },
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 8.dp),
            value = port,
            onValueChange = {
                port = it
                onPortChanged(it)
            }
        )
        OutlinedTextField(
            label = {
                Text(text = stringResource(id = R.string.favorite_dialog_pass_hint))
            },
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 8.dp),
            value = pass,
            onValueChange = {
                pass = it
                onPasswordChanged(it)
            },
            visualTransformation = if (passwordVisible) {
                VisualTransformation.None
            } else {
                PasswordVisualTransformation()
            },
            trailingIcon = {
                IconButton(onClick = { passwordVisible = !passwordVisible }) {
                    Icon(
                        painter = painterResource(
                            id = when {
                                passwordVisible -> R.drawable.ic_visibility
                                else -> R.drawable.ic_visibility_off
                            }
                        ),
                        contentDescription = ""
                    )
                }
            }
        )
    }
}

@ExperimentalMaterial3Api
@Preview
@Composable
fun FavoriteDialogPreview() {
    VlcRemoteTheme {
        Surface {
            FavoriteDialogBody(
                favorite = Favorite(
                    0,
                    "Favorite 1",
                    "Server",
                    1234,
                    "password"
                ),
                onAliasChanged = {},
                onServerChanged = {},
                onPortChanged = {},
                onPasswordChanged = {},
            )
        }
    }
}