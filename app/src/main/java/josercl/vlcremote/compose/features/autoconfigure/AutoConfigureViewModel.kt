package josercl.vlcremote.compose.features.autoconfigure

import android.content.SharedPreferences
import androidx.core.content.edit
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import josercl.vlcremote.CoroutineContextDispatcher
import josercl.vlcremote.PREF_HOST
import josercl.vlcremote.PREF_PASS
import josercl.vlcremote.PREF_PORT
import josercl.vlcremote.extensions.isServer
import josercl.vlcremote.net.Scanner
import kotlinx.coroutines.Job
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import java.net.InetAddress
import javax.inject.Inject

data class AutoConfigureState(
    val list: List<InetAddress> = emptyList(),
    val loading: Boolean = false,
    val finished: Boolean = false,
    val hasPermission: Boolean = false,
    val max: Int = 0,
    val step: Int = 0
)

@HiltViewModel
class AutoConfigureViewModel @Inject constructor(
    private val preferences: SharedPreferences,
    private val networkScanner: Scanner,
    private val contextDispatcher: CoroutineContextDispatcher
) : ViewModel() {
    private val _state = MutableStateFlow(AutoConfigureState())
    val state = _state.asStateFlow()

    override fun onCleared() {
        this.cancelScan()
        super.onCleared()
    }

    fun processEvent(event: AutoConfigurationEvent) {
        when(event){
            AutoConfigurationEvent.StartScan -> startScan()
            AutoConfigurationEvent.StopScan -> cancelScan()
            is AutoConfigurationEvent.UpdateWifiStatePermission -> updateHasPermissions(event.hasPermission)
            is AutoConfigurationEvent.UpdateSettings -> updateSettings(event.addr)
        }
    }

    private var scanJob: Job? = null
    private fun startScan() {
        scanJob = viewModelScope.launch(contextDispatcher.IO) {
            val ips = networkScanner.getIpRange()

            _state.update {
                it.copy(
                    loading = true,
                    finished = false,
                    list = emptyList(),
                    max = ips.size,
                    step = 0
                )
            }

            ips.map {
                async(context = this@launch.coroutineContext) {
                    val result: InetAddress? = if (it.isServer()) {
                        it.hostName
                        it
                    } else {
                        null
                    }

                    if (result != null) {
                        _state.update {
                            val newList = it.list.toMutableList()
                            newList += result

                            it.copy(
                                list = newList,
                                step = it.step + 1
                            )
                        }
                    } else {
                        _state.update {
                            it.copy (step = it.step + 1)
                        }
                    }
                }
            }.awaitAll()

            _state.update {
                it.copy(
                    loading = false,
                    finished = true,
                )
            }
        }
    }

    private fun updateHasPermissions(hasPermission: Boolean) =
        _state.update { it.copy(hasPermission = hasPermission) }

    private fun cancelScan() {
        _state.update { it.copy(loading = false) }
        scanJob?.cancel()
    }

    private fun updateSettings(addr: String) {
        cancelScan()

        preferences.edit(true) {
            putString(PREF_HOST, addr)
            putString(PREF_PORT, "8080")
            putString(PREF_PASS, "change_me")
        }
    }
}