package josercl.vlcremote.compose.features.equalizer

import android.content.res.Configuration
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.statusBarsPadding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.ExtendedFloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Switch
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import josercl.vlcremote.compose.NavEvent
import josercl.vlcremote.compose.R
import josercl.vlcremote.compose.composable.VlcRemoteTopBar
import josercl.vlcremote.compose.ui.theme.VlcRemoteTheme
import josercl.vlcremote.model.net.Equalizer

@ExperimentalLayoutApi
@ExperimentalFoundationApi
@ExperimentalMaterial3Api
@Composable
fun EqualizerComposable(
    state: EqualizerState,
    onNavEvent: (NavEvent) -> Unit,
    onEqualizerEvent: (EqualizerEvent) -> Unit,
) {
    val scrollBehavior = TopAppBarDefaults.pinnedScrollBehavior()

    var showPresetsDialog by remember { mutableStateOf(false) }

    var presetButtonText by remember { mutableStateOf<String?>(null) }

    var enabled by remember(state) { mutableStateOf(state.enabled) }

    if (showPresetsDialog) {
        AlertDialog(
            onDismissRequest = { showPresetsDialog = false },
            dismissButton = {
                TextButton(onClick = { showPresetsDialog = false }) {
                    Text(text = stringResource(id = R.string.cancel))
                }
            },
            confirmButton = {},
            text = {
                LazyColumn {
                    val presets = Equalizer.Presets.getAll().map(Equalizer::presetName)
                    itemsIndexed(presets) { index, preset ->
                        Text(
                            modifier = Modifier
                                .fillMaxWidth()
                                .clickable {
                                    onEqualizerEvent(EqualizerEvent.SetPreset(index))
                                    presetButtonText = preset
                                    showPresetsDialog = false
                                }
                                .padding(16.dp),
                            text = preset
                        )
                    }
                }
            }
        )
    }

    Scaffold(
        modifier = Modifier.nestedScroll(connection = scrollBehavior.nestedScrollConnection),
        topBar = {
            VlcRemoteTopBar(
                modifier = Modifier.statusBarsPadding(),
                title = stringResource(id = R.string.equalizer_title),
                onNavigationIconClick = { onNavEvent(NavEvent.GoBack) },
            )
        },
        floatingActionButton = {
            ExtendedFloatingActionButton(
                onClick = {
                    if (enabled) {
                        showPresetsDialog = true
                    }
                }
            ) {
                Text(
                    text = presetButtonText ?: stringResource(id = R.string.equalizer_presets_button_text)
                )
            }
        }
    ) { pad ->
        LazyColumn(
            modifier = Modifier
                .fillMaxSize()
                .padding(pad),
            contentPadding = PaddingValues(start = 16.dp, end = 16.dp, bottom = 88.dp),
            verticalArrangement = Arrangement.spacedBy(16.dp),
        ) {
            stickyHeader {
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .background(color = MaterialTheme.colorScheme.background)
                        .clickable {
                            enabled = !enabled
                            onEqualizerEvent(EqualizerEvent.ToggleEqualizer)
                        },
                    verticalAlignment = Alignment.CenterVertically,
                ) {
                    Text(text = stringResource(id = R.string.equalizer_enabled_switch_label))
                    Spacer(modifier = Modifier.width(16.dp))
                    Switch(
                        checked = enabled,
                        onCheckedChange = {
                            enabled = it
                            onEqualizerEvent(EqualizerEvent.ToggleEqualizer)
                        },
                        thumbContent = {
                            Icon(
                                modifier = Modifier.size(16.dp),
                                painter = painterResource(id = when(enabled) {
                                    true -> R.drawable.ic_check
                                    else -> R.drawable.ic_close
                                }),
                                contentDescription= null,
                            )
                        }
                    )
                }
            }
            item {
                EqualizerBand(
                    label = Equalizer.BANDS.first(),
                    value = state.equalizer.preamp,
                    enabled = enabled,
                    onValueChanged = {
                        onEqualizerEvent(EqualizerEvent.SetPreamp(it))
                    },
                )
            }
            itemsIndexed(items = state.equalizer.bands.toTypedArray()) { index: Int, band: Double ->
                EqualizerBand(
                    label = Equalizer.BANDS[index + 1],
                    value = band,
                    enabled = enabled,
                    onValueChanged = {
                        onEqualizerEvent(EqualizerEvent.SetBand(index, it))
                    }
                )
            }
        }
    }
}

@ExperimentalLayoutApi
@ExperimentalFoundationApi
@ExperimentalMaterial3Api
@Preview(group = "Light")
@Preview(uiMode = Configuration.UI_MODE_NIGHT_YES, group = "Dark")
@Composable
fun EqualizerComposablePreview() {
    VlcRemoteTheme {
        EqualizerComposable(
            state = EqualizerState(),
            onNavEvent = {},
            onEqualizerEvent = {}
        )
    }
}