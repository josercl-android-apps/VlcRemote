package josercl.vlcremote.compose.features.playlist

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import josercl.vlcremote.CoroutineContextDispatcher
import josercl.vlcremote.model.enum.VlcSortMode
import josercl.vlcremote.model.enum.VlcSortOrder
import josercl.vlcremote.model.net.PlaylistNode
import josercl.vlcremote.net.IVlcService
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

data class PlaylistState(
    val playlist: List<PlaylistNode> = emptyList(),
    val deletedItems: List<PlaylistNode> = emptyList(),
    val loadingPlaylist: Boolean = false
)

@HiltViewModel
class PlaylistViewModel @Inject constructor(
    private val vlcService: IVlcService,
    private val contextDispatcher: CoroutineContextDispatcher
) : ViewModel() {
    private val _state = MutableStateFlow(PlaylistState())
    val state = _state.asStateFlow()

    init {
        retrievePlaylist()
    }

    fun processEvent(event: PlaylistEvent) {
        when (event) {
            PlaylistEvent.ClearPlaylist -> clearPlaylist()
            PlaylistEvent.LoadPlaylist -> retrievePlaylist()
            is PlaylistEvent.PlayNow -> playItem(event.item)
            is PlaylistEvent.RemoveFromPlaylist -> removeItem(event.item)
            is PlaylistEvent.DeleteItems -> deleteItems(event.items)
            PlaylistEvent.RestoreDeletedItems -> restoreDeletedItems()
        }
    }

    private fun retrievePlaylist() {
        viewModelScope.launch(contextDispatcher.IO) {
            _state.value = _state.value.copy(loadingPlaylist = true)
            try {
                val response = vlcService.getPlaylist()
                _state.update { state ->
                    state.copy(
                        loadingPlaylist = false,
                        playlist = response.children?.firstOrNull { it.id == 1 }?.children.orEmpty()
                    )
                }
            } catch (e: Exception) {
                Timber.e(e)
                _state.update { it.copy(loadingPlaylist = false) }
            }
        }
    }

    fun sortPlaylist(order: VlcSortOrder, mode: VlcSortMode) {
        viewModelScope.launch(contextDispatcher.IO) {
            try {
                vlcService.sortPlaylist(order, mode)
            } catch (e: Exception) {
                Timber.e(e)
            } finally {
                retrievePlaylist()
            }
        }
    }

    private fun clearPlaylist() {
        viewModelScope.launch(contextDispatcher.IO) {
            try {
                vlcService.clearPlaylist()
            } catch (e: Exception) {
                Timber.e(e)
            } finally {
                retrievePlaylist()
            }
        }
    }

    private fun playItem(item: PlaylistNode) {
        viewModelScope.launch(contextDispatcher.IO) {
            try {
                vlcService.playItemFromPlaylist(item.id)

                val newPlaylist = state.value.playlist.map {
                    if (it.id == item.id) {
                        it.copy(current = "current")
                    } else {
                        it.copy(current = null)
                    }
                }

                _state.update { it.copy(playlist = newPlaylist) }
            } catch (e: Exception) {
                Timber.e(e)
            }
        }
    }

    private fun removeItem(item: PlaylistNode) {
        this.removeItemAt(_state.value.playlist.indexOfFirst { it.id == item.id })
    }

    private fun removeItemAt(position: Int) {
        if (position != -1) {
            val deletedItems = _state.value.deletedItems.toMutableList()
            val itemToDelete = _state.value.playlist[position].apply {
                this@apply.position = position
            }

            if (deletedItems.add(itemToDelete)) {
                val newPlaylist = _state.value.playlist.toMutableList().apply { removeAt(position) }

                _state.update {
                    it.copy(
                        playlist = newPlaylist,
                        deletedItems = deletedItems
                    )
                }
                //playlist.value = newPlaylist
                //this@MainFragmentViewModel.deletedItems.value = deletedItems
            }
        }
    }

    private fun deleteItems(items: List<PlaylistNode>) {
        viewModelScope.launch(contextDispatcher.IO) {
            try {
                items.map {
                    async { vlcService.removeItemFromPlaylist(it.id) }
                }.awaitAll()

                _state.update {
                    it.copy(
                        playlist = it.playlist - items,
                        deletedItems = emptyList()
                    )
                }
                //deletedItems.postValue(arrayListOf())
            } catch (e: Exception) {
                Timber.e(e)
            } finally {
                retrievePlaylist()
            }
        }
    }

    private fun restoreDeletedItems() {
        val newPlaylist = _state.value.playlist.toMutableList()
        _state.value.deletedItems
            .reversed()
            .forEach {
                val restoredPosition = it.position
                it.position = -1
                newPlaylist.add(restoredPosition, it)
            }

        _state.update {
            it.copy(
                playlist = newPlaylist,
                deletedItems = emptyList()
            )
        }
    }
}