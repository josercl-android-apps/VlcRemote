package josercl.vlcremote.compose.features.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import josercl.vlcremote.CoroutineContextDispatcher
import josercl.vlcremote.model.net.StatusResponse
import josercl.vlcremote.model.net.Subtitle
import josercl.vlcremote.net.IVlcService
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

data class MainFragmentState(
    val status: StatusResponse = StatusResponse(),
    val draggingVolume: Boolean = false,
    val draggingProgress: Boolean = false,
    val error: Throwable? = null,
    val connected: Boolean = true,
)

@ExperimentalCoroutinesApi
@HiltViewModel
class MainFragmentViewModel @Inject constructor(
    private val vlcService: IVlcService,
    private val contextDispatcher: CoroutineContextDispatcher,
) : ViewModel() {
    private val _state = MutableStateFlow(MainFragmentState())
    val state = _state.asStateFlow()

    fun getStatus() = flow<Unit> {
        while (true) {
            runCatching {
                _state.update { it.copy(status = vlcService.getStatus()) }
            }.getOrElse {
                if (it !is CancellationException) {
                    _state.update { s ->
                        s.copy(status = StatusResponse(), error = it, connected = false)
                    }
                }
                Timber.e(it)
            }

            delay(1_000)
        }
    }.flowOn(contextDispatcher.IO)

    fun processEvent(event: MainControlsEvent) {
        when (event) {
            MainControlsEvent.Stop -> stop()
            MainControlsEvent.Play -> play()
            MainControlsEvent.Pause -> pause()
            MainControlsEvent.Random -> random()
            MainControlsEvent.Repeat -> repeat()
            is MainControlsEvent.SeekTo -> seekTo(event.value)
            is MainControlsEvent.SetVolume -> setVolume(event.value)
            MainControlsEvent.SkipNext -> next()
            MainControlsEvent.SkipPrev -> prev()
            MainControlsEvent.ToggleFullScreen -> fullScreen()
            is MainControlsEvent.SetSubtitle -> setSubtitle(event.subtitle)
            is MainControlsEvent.SetSubtitleDelay -> setSubtitleDelay(event.delay)
            is MainControlsEvent.DraggingProgress -> _state.update { it.copy(draggingProgress = event.dragging) }
            is MainControlsEvent.DraggingVolume -> _state.update { it.copy(draggingVolume = event.dragging) }
        }
    }

    private fun stop() = execute(vlcService::stop)

    private fun pause() = execute(vlcService::pause)

    private fun play() = execute(vlcService::play)

    private fun fullScreen() = execute(vlcService::setFullScreen)

    private fun prev() = execute(vlcService::previous)

    private fun next() = execute(vlcService::next)

    private fun repeat() = execute(vlcService::repeat)

    private fun random() = execute(vlcService::random)

    private fun seekTo(position: String) = execute { vlcService.seekTo(position) }

    private fun setVolume(volume: Int) = execute { vlcService.setVolume(volume) }

    private fun setSubtitle(subtitle: Subtitle) = execute { vlcService.setSubtitle(subtitle.stream) }

    private fun setSubtitleDelay(delay: Double) = execute { vlcService.setSubtitleDelay(delay) }

    private fun execute(function: suspend () -> Unit) {
        viewModelScope.launch(contextDispatcher.IO) {
            try {
                function()
            } catch (e: Exception) {
                Timber.e(e)
            }
        }
    }
}
