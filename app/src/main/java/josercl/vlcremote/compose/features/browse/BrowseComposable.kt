package josercl.vlcremote.compose.features.browse

import android.content.res.Configuration
import androidx.activity.compose.BackHandler
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.statusBarsPadding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.LocalContentColor
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.ModalBottomSheet
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import josercl.vlcremote.compose.NavEvent
import josercl.vlcremote.compose.R
import josercl.vlcremote.compose.composable.VlcRemoteTopBar
import josercl.vlcremote.compose.ui.theme.VlcRemoteTheme
import josercl.vlcremote.model.net.File

@ExperimentalLayoutApi
@ExperimentalMaterial3Api
@Composable
fun BrowseComposable(
    state: BrowserState,
    onNavEvent: (NavEvent) -> Unit,
    onBrowseEvent: (BrowseEvent) -> Unit,
    onUpdatePlaylist: () -> Unit,
) {
    var showBottomSheet by remember { mutableStateOf(false) }

    var folderLongClicked by remember { mutableStateOf(false) }

    var currentFile by remember { mutableStateOf<File?>(null) }

    BackHandler(showBottomSheet || state.hasFilesInHistory) {
        if (showBottomSheet) {
            showBottomSheet = false
        } else if (state.hasFilesInHistory) {
            onBrowseEvent(BrowseEvent.OpenFolder(File.PREVIOUS_FOLDER))
        }
    }

    if (showBottomSheet) {
        ModalBottomSheet(
            onDismissRequest = { showBottomSheet = false },
            dragHandle = null,
        ) {
            CompositionLocalProvider(LocalContentColor provides MaterialTheme.colorScheme.onSurfaceVariant) {
                if (folderLongClicked) {
                    FolderActions(
                        fileName = currentFile?.name.orEmpty(),
                        onEnqueue = {
                            onBrowseEvent(BrowseEvent.EnqueueFolder(currentFile!!) {
                                onUpdatePlaylist()
                            })
                            showBottomSheet = false
                            folderLongClicked = false
                        },
                        onDismiss = {
                            showBottomSheet = false
                            folderLongClicked = false
                        }
                    )
                } else {
                    FileActions(
                        fileName = currentFile?.name.orEmpty(),
                        onPlayNow = {
                            onBrowseEvent(BrowseEvent.FileEvent.PlayNow(currentFile!!))
                            currentFile = null
                            showBottomSheet = false
                            onUpdatePlaylist()
                            onNavEvent(NavEvent.GoBack)
                        },
                        onEnqueue = {
                            onBrowseEvent(BrowseEvent.FileEvent.Enqueue(currentFile!!))
                            currentFile = null
                            showBottomSheet = false
                            onUpdatePlaylist()
                        }
                    )
                }
            }
        }
    }

    Scaffold(
        modifier = Modifier,
        topBar = {
            VlcRemoteTopBar(
                modifier = Modifier.statusBarsPadding(),
                title = stringResource(id = R.string.browse_title),
                onNavigationIconClick = { onNavEvent(NavEvent.GoBack) },
            )
        },
    ) { pad ->
        Box(
            modifier = Modifier
                .fillMaxSize()
                .padding(pad),
            contentAlignment = Alignment.Center
        ) {
            LazyColumn(
                modifier = Modifier.fillMaxSize(),
            ) {
                itemsIndexed(state.files) { index, file ->
                    if (index > 0) {
                        HorizontalDivider()
                    }
                    Row(
                        modifier = Modifier
                            .fillMaxWidth()
                            .pointerInput(file) {
                                detectTapGestures(
                                    onLongPress = {
                                        if (file.isDir() && file.name != File.PREVIOUS_FOLDER.name) {
                                            folderLongClicked = true
                                            currentFile = file
                                            showBottomSheet = true
                                        }
                                    },
                                    onTap = {
                                        folderLongClicked = false
                                        when {
                                            file.isDir() -> onBrowseEvent(
                                                BrowseEvent.OpenFolder(
                                                    file
                                                )
                                            )

                                            else -> {
                                                currentFile = file
                                                showBottomSheet = true
                                            }
                                        }
                                    }
                                )
                            }
                            //.clickable {}
                            .padding(all = 16.dp),
                        verticalAlignment = Alignment.CenterVertically,
                    ) {
                        Icon(
                            painter = painterResource(
                                id = when {
                                    file.isAudio() -> R.drawable.ic_file_type_music
                                    file.isVideo() -> R.drawable.ic_file_type_video
                                    file.isImage() -> R.drawable.ic_file_type_image
                                    file.isText() -> R.drawable.ic_file_type_text
                                    file.isDir() -> R.drawable.ic_file_type_folder
                                    else -> R.drawable.ic_file_type_plain
                                }
                            ),
                            contentDescription = ""
                        )
                        Spacer(modifier = Modifier.width(32.dp))
                        Text(
                            text = file.name,
                            maxLines = 1,
                            overflow = TextOverflow.Ellipsis,
                            style = MaterialTheme.typography.bodyLarge
                        )
                    }
                }
            }
            AnimatedVisibility(visible = state.loading) {
                CircularProgressIndicator(
                    color = MaterialTheme.colorScheme.secondary
                )
            }
        }
    }
}

@ExperimentalLayoutApi
@ExperimentalMaterial3Api
@Preview(group = "Light")
@Preview(uiMode = Configuration.UI_MODE_NIGHT_YES, group = "Dark")
@Composable
fun BrowseComposablePreview() {
    VlcRemoteTheme {
        BrowseComposable(
            state = BrowserState(
                loading = true,
                history = emptyList(),
                files = (0..20).map {
                    File("", "file.mpg", 123L, "File Name $it", "")
                }
            ),
            onNavEvent = {},
            onBrowseEvent = {},
            onUpdatePlaylist = {}
        )
    }
}