package josercl.vlcremote.compose.features.main

import android.content.res.Configuration.UI_MODE_NIGHT_YES
import androidx.activity.compose.BackHandler
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.IconButtonDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.ModalBottomSheet
import androidx.compose.material3.NavigationBar
import androidx.compose.material3.NavigationBarItem
import androidx.compose.material3.OutlinedIconButton
import androidx.compose.material3.OutlinedIconToggleButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Slider
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableLongStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import josercl.vlcremote.Routes
import josercl.vlcremote.compose.NavEvent
import josercl.vlcremote.compose.R
import josercl.vlcremote.compose.ui.theme.VlcRemoteTheme
import josercl.vlcremote.extensions.formatSeconds
import josercl.vlcremote.extensions.plus
import josercl.vlcremote.extensions.toInt
import josercl.vlcremote.model.net.Category
import josercl.vlcremote.model.net.Information
import josercl.vlcremote.model.net.Meta
import josercl.vlcremote.model.net.StatusResponse
import josercl.vlcremote.model.net.Subtitle
import kotlin.math.roundToInt
import kotlin.math.roundToLong

@ExperimentalLayoutApi
@ExperimentalAnimationApi
@ExperimentalComposeUiApi
@ExperimentalMaterial3Api
@Composable
fun MainComposable(
    state: MainFragmentState = MainFragmentState(),
    toJson: (Any) -> String,
    onNavEvent: (NavEvent) -> Unit = {},
    onMainEvent: (MainControlsEvent) -> Unit = {}
) {
    var showSubtitlesActions by remember { mutableStateOf(false) }

    BackHandler(enabled = showSubtitlesActions) {
        showSubtitlesActions = false
    }

    if (showSubtitlesActions) {
        ModalBottomSheet(
            onDismissRequest = { showSubtitlesActions = false},
            dragHandle = null,
        ) {
            SubtitleActions(
                delay = state.status.subtitleDelay,
                subtitles = state.status.information.category.subtitles,
                onDelayChanged = { onMainEvent(MainControlsEvent.SetSubtitleDelay(it)) },
                onSubtitleSelected = {
                    onMainEvent(MainControlsEvent.SetSubtitle(it))
                    showSubtitlesActions = false
                }
            )
        }
    }

    Scaffold(
        modifier = Modifier.fillMaxSize(),
        bottomBar = {
            MainNavigationBar(onNavEvent)
        }
    ) { pad ->
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(pad + PaddingValues(start = 16.dp, end = 16.dp, top = 32.dp)),
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {
            Text(
                text = state.status.meta.title,
                modifier = Modifier.fillMaxWidth(),
                fontWeight = FontWeight.Bold,
                maxLines = 1,
                textAlign = TextAlign.Center,
                overflow = TextOverflow.Ellipsis,
            )
            Text(
                text = state.status.meta.filename,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(top = 8.dp),
                textAlign = TextAlign.Center,
                maxLines = 1,
                overflow = TextOverflow.Ellipsis
            )
            Spacer(modifier = Modifier.height(24.dp))
            ProgressControls(
                status = state.status,
                onBackwardClicked = { onMainEvent(MainControlsEvent.SeekTo("-10S")) },
                onForwardClicked = { onMainEvent(MainControlsEvent.SeekTo("+30S")) },
                onDragFinished = { onMainEvent(MainControlsEvent.SeekTo("$it")) }
            )
            Spacer(modifier = Modifier.weight(1f))
            PlaybackControl(
                status = state.status,
                onToggleRepeat = { onMainEvent(MainControlsEvent.Repeat) },
                onSkipPrevious = { onMainEvent(MainControlsEvent.SkipPrev) },
                onSkipNext = { onMainEvent(MainControlsEvent.SkipNext) },
                onTogglePlayPause = {
                    if (state.status.isPlaying) {
                        onMainEvent(MainControlsEvent.Pause)
                    } else {
                        onMainEvent(MainControlsEvent.Play)
                    }
                },
                onToggleShuffle = { onMainEvent(MainControlsEvent.Random) }
            )
            Spacer(modifier = Modifier.height(24.dp))
            VolumeControls(
                volume = state.status.volume,
                onVolumeChanged = { onMainEvent(MainControlsEvent.SetVolume(it)) }
            )
            Spacer(modifier = Modifier.height(24.dp))
            OtherControls(
                state = state,
                onToggleMaximize = { onMainEvent(MainControlsEvent.ToggleFullScreen) },
                onEqualizerClicked = {
                    onNavEvent(
                        NavEvent.GoTo(
                            "${Routes.EQUALIZER}/${toJson(state.status.equalizer)}/${state.status.audiofilters.equalizer.toInt()}"
                        )
                    )
                },
                onSubtitlesClicked = {
                    showSubtitlesActions = true
                }
            )
            Spacer(modifier = Modifier.weight(1f))
        }
    }
}

@Composable
private fun ProgressControls(
    status: StatusResponse,
    onBackwardClicked: () -> Unit,
    onForwardClicked: () -> Unit,
    onDragFinished: (Long) -> Unit,
) {
    val progress by remember(status) { mutableLongStateOf(status.time) }
    var progressDragging by remember { mutableLongStateOf(0L) }

    Column(
        modifier = Modifier.fillMaxWidth(),
    ) {
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Text(
                text = progress.formatSeconds(),
                style = MaterialTheme.typography.bodyMedium
            )
            Text(
                text = status.getFormattedTotalTime(),
                style = MaterialTheme.typography.bodyMedium
            )
        }
        Slider(
            modifier = Modifier.fillMaxWidth(),
            value = progress.toFloat(),
            valueRange = 0f..status.length.toFloat(),
            onValueChange = { progressDragging = it.roundToLong() },
            onValueChangeFinished = {
                onDragFinished(progressDragging)
            }
        )
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            OutlinedIconButton(onClick = { onBackwardClicked() }) {
                Icon(
                    painter = painterResource(id = R.drawable.ic_backward_10),
                    contentDescription = null,
                )
            }
            OutlinedIconButton(onClick = { onForwardClicked() }) {
                Icon(
                    painter = painterResource(id = R.drawable.ic_forward_30),
                    contentDescription = null,
                )
            }
        }
    }

}

@Composable
fun VolumeControls(
    volume: Int,
    onVolumeChanged: (Int) -> Unit,
) {
    var vol by remember(volume) { mutableIntStateOf(volume) }

    Row(
        modifier = Modifier.fillMaxWidth(),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.spacedBy(16.dp)
    ) {
        IconButton(onClick = {}) {
            Icon(
                painter = painterResource(id = R.drawable.ic_volume_up),
                contentDescription = null
            )
        }
        Text(text = "${"${vol * 100 / 256}".padStart(3, ' ')} %")
        Slider(
            modifier = Modifier.weight(1f),
            value = vol.toFloat(),
            valueRange = 0f..256f,
            onValueChange = {
                vol = it.roundToInt()
            },
            onValueChangeFinished = {
                onVolumeChanged(vol)
            }
        )
    }
}


@Composable
private fun PlaybackControl(
    status: StatusResponse,
    onToggleRepeat: () -> Unit,
    onSkipPrevious: () -> Unit,
    onSkipNext: () -> Unit,
    onTogglePlayPause: () -> Unit,
    onToggleShuffle: () -> Unit,
) {
    Row(
        modifier = Modifier.fillMaxWidth(),
        horizontalArrangement = Arrangement.SpaceEvenly,
        verticalAlignment = Alignment.CenterVertically,
    ) {
        OutlinedIconToggleButton(
            checked = status.repeat,
            onCheckedChange = { onToggleRepeat() },
            colors = IconButtonDefaults.outlinedIconToggleButtonColors(
                checkedContainerColor = MaterialTheme.colorScheme.primary,
                checkedContentColor = MaterialTheme.colorScheme.onPrimary,
            )
        ) {
            Icon(
                painter = painterResource(id = R.drawable.ic_repeat),
                contentDescription = null
            )
        }
        IconButton(
            onClick = { onSkipPrevious() }
        ) {
            Icon(
                painter = painterResource(id = R.drawable.ic_skip_previous),
                contentDescription = null
            )
        }
        OutlinedIconButton(
            modifier = Modifier.size(96.dp),
            onClick = { onTogglePlayPause() },
        ) {
            Icon(
                modifier = Modifier.size(48.dp),
                painter = painterResource(id = if (status.isPlaying) R.drawable.ic_pause else R.drawable.ic_play),
                contentDescription = null
            )
        }
        IconButton(
            onClick = { onSkipNext() },
        ) {
            Icon(
                painter = painterResource(id = R.drawable.ic_skip_next),
                contentDescription = null
            )
        }
        OutlinedIconToggleButton(
            checked = status.random,
            onCheckedChange = { onToggleShuffle() },
            colors = IconButtonDefaults.outlinedIconToggleButtonColors(
                checkedContainerColor = MaterialTheme.colorScheme.primary,
                checkedContentColor = MaterialTheme.colorScheme.onPrimary,
            )
        ) {
            Icon(
                painter = painterResource(id = R.drawable.ic_shuffle),
                contentDescription = null
            )
        }
    }
}

@Composable
fun OtherControls(
    state: MainFragmentState,
    onToggleMaximize: () -> Unit,
    onSubtitlesClicked: () -> Unit,
    onEqualizerClicked: () -> Unit,
) {
    Row(
        modifier = Modifier.fillMaxWidth(),
        horizontalArrangement = Arrangement.SpaceEvenly
    ) {
        OutlinedIconButton(
            onClick = { onToggleMaximize() },
        ) {
            Icon(
                painter = painterResource(
                    id = if (state.status.fullscreen) R.drawable.ic_minimize else R.drawable.ic_maximize
                ),
                contentDescription = null
            )
        }

        AnimatedVisibility(visible = state.status.hasSubtitles) {
            OutlinedIconButton(
                onClick = { onSubtitlesClicked() },
            ) {
                Icon(
                    painter = painterResource(id = R.drawable.ic_subtitles),
                    contentDescription = null
                )
            }
        }

        OutlinedIconButton(
            onClick = { onEqualizerClicked() },
        ) {
            Icon(
                painter = painterResource(id = R.drawable.ic_equalizer),
                contentDescription = null
            )
        }
    }
}

@Composable
private fun MainNavigationBar(onNavEvent: (NavEvent) -> Unit) {
    NavigationBar {
        NavigationBarItem(
            selected = false,
            onClick = {
                onNavEvent(NavEvent.GoTo(Routes.FAVORITES))
            },
            icon = {
                Icon(
                    painter = painterResource(id = R.drawable.ic_star),
                    contentDescription = ""
                )
            },
            label = {
                Text(text = stringResource(id = R.string.favorites_title))
            }
        )
        NavigationBarItem(
            selected = false,
            onClick = {
                onNavEvent(NavEvent.GoTo(Routes.PLAYLIST))
            },
            icon = {
                Icon(
                    painter = painterResource(id = R.drawable.ic_playlist),
                    contentDescription = ""
                )
            },
            label = {
                Text(text = stringResource(id = R.string.playlist_title))
            }
        )
        NavigationBarItem(
            selected = false,
            onClick = {
                onNavEvent(NavEvent.GoTo(Routes.SETTINGS))
            },
            icon = {
                Icon(
                    painter = painterResource(id = R.drawable.ic_settings),
                    contentDescription = ""
                )
            },
            label = {
                Text(text = stringResource(id = R.string.settings_title))
            }
        )
    }
}

@ExperimentalLayoutApi
@ExperimentalAnimationApi
@ExperimentalComposeUiApi
@ExperimentalMaterial3Api
@Preview(group = "Light")
@Preview(uiMode = UI_MODE_NIGHT_YES, group = "Dark")
@Composable
fun MainComposablePreview() {
    VlcRemoteTheme {
        MainComposable(
            state = MainFragmentState(
                status = StatusResponse(
                    length = 100,
                    time = 65,
                    volume = 128,
                    information = Information(
                        category = Category(
                            meta = Meta(
                                filename = "Filename",
                                title = "Title"
                            ),
                            subtitles = listOf(Subtitle("", "", -1))
                        )
                    )
                )
            ),
            toJson = { "" }
        )
    }
}