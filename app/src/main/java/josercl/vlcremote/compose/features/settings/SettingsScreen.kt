package josercl.vlcremote.compose.features.settings

import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.runtime.Composable
import androidx.hilt.navigation.compose.hiltViewModel
import josercl.vlcremote.compose.NavEvent

@ExperimentalMaterial3Api
@ExperimentalLayoutApi
@Composable
fun SettingsScreen(
    viewModel: SettingsViewModel = hiltViewModel(),
    onNavEvent: (NavEvent) -> Unit
) {
    SettingsComposable(
        onNavEvent = onNavEvent,
        onSettingsEvent = viewModel::processEvent
    )
}