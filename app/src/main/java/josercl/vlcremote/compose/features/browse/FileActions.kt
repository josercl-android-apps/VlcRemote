package josercl.vlcremote.compose.features.browse

import android.content.res.Configuration.UI_MODE_NIGHT_YES
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Divider
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.LocalContentColor
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import josercl.vlcremote.compose.R
import josercl.vlcremote.compose.ui.theme.VlcRemoteTheme

@Composable
fun FileActions(
    fileName: String,
    contentColor: Color = LocalContentColor.current,
    onPlayNow: () -> Unit,
    onEnqueue: () -> Unit
) {
    Column(
        modifier = Modifier.fillMaxWidth()
    ) {
        Text(
            text = fileName,
            maxLines = 1,
            overflow = TextOverflow.Ellipsis,
            style = MaterialTheme.typography.bodyMedium,
            modifier = Modifier
                .fillMaxWidth()
                .padding(all = 16.dp),
            fontWeight = FontWeight.Bold,
            color = contentColor,
        )
        HorizontalDivider(
            thickness = 1.dp,
            color = MaterialTheme.colorScheme.primary
        )
        FileActionButton(
            icon = R.drawable.ic_play,
            text = R.string.file_action_play_now,
            onClick = onPlayNow,
            contentColor = contentColor,
            modifier = Modifier.fillMaxWidth()
        )
        FileActionButton(
            icon = R.drawable.ic_playlist_add,
            text = R.string.file_action_enqueue,
            onClick = onEnqueue,
            contentColor = contentColor,
            modifier = Modifier.fillMaxWidth()
        )
    }
}

@Composable
fun FileActionButton(
    modifier: Modifier = Modifier,
    @DrawableRes icon: Int,
    @StringRes text: Int,
    contentColor: Color = LocalContentColor.current,
    onClick: () -> Unit,
) {
    TextButton(
        contentPadding = PaddingValues(16.dp),
        modifier = modifier,
        onClick = onClick,
        shape = RectangleShape,
        colors = ButtonDefaults.textButtonColors(
            contentColor = contentColor
        )
    ) {
        Icon(
            painter = painterResource(id = icon),
            contentDescription = "",
            tint = contentColor
        )
        Spacer(modifier = Modifier.width(32.dp))
        Text(
            modifier = Modifier.weight(1f),
            text = stringResource(id = text),
            style = MaterialTheme.typography.bodyMedium,
            color = contentColor
        )
    }
}

@Preview
@Preview(uiMode = UI_MODE_NIGHT_YES)
@Composable
fun FileActionsPreview() {
    VlcRemoteTheme {
        Surface {
            FileActions(
                "File name",
                onPlayNow = {},
                onEnqueue = {}
            )
        }
    }
}