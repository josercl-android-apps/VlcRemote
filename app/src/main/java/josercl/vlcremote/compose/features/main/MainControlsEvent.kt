package josercl.vlcremote.compose.features.main

import josercl.vlcremote.model.net.Subtitle

sealed interface MainControlsEvent {
    object Stop: MainControlsEvent
    object Play: MainControlsEvent
    object Pause: MainControlsEvent
    object SkipNext: MainControlsEvent
    object SkipPrev: MainControlsEvent
    object ToggleFullScreen: MainControlsEvent
    object Random: MainControlsEvent
    object Repeat: MainControlsEvent
    data class SeekTo(val value: String): MainControlsEvent
    data class SetVolume(val value: Int): MainControlsEvent
    data class SetSubtitle(val subtitle: Subtitle): MainControlsEvent
    data class SetSubtitleDelay(val delay: Double): MainControlsEvent
    data class DraggingVolume(val dragging: Boolean): MainControlsEvent
    data class DraggingProgress(val dragging: Boolean): MainControlsEvent
}

