package josercl.vlcremote.compose.features.main

import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.hilt.navigation.compose.hiltViewModel
import josercl.vlcremote.compose.NavEvent
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.launchIn

@ExperimentalLayoutApi
@ExperimentalAnimationApi
@ExperimentalMaterial3Api
@ExperimentalComposeUiApi
@ExperimentalCoroutinesApi
@Composable
fun MainScreen(
    viewModel: MainFragmentViewModel = hiltViewModel(),
    toJson: (Any) -> String,
    onNavEvent: (NavEvent) -> Unit
) {
    val state by viewModel.state.collectAsState()

    LaunchedEffect(key1 = true) {
        viewModel.getStatus().launchIn(this)
    }

    MainComposable(
        state = state,
        toJson = toJson,
        onNavEvent = onNavEvent,
        onMainEvent = viewModel::processEvent
    )
}