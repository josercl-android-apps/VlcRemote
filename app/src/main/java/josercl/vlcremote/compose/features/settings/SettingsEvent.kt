package josercl.vlcremote.compose.features.settings

import josercl.vlcremote.model.Favorite

sealed class SettingsEvent {
    data class SaveFavorite(val favorite: Favorite): SettingsEvent()
    object ResetSavedFavorite: SettingsEvent()
}
