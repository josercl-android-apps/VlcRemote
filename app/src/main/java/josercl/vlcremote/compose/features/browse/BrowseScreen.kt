package josercl.vlcremote.compose.features.browse

import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.hilt.navigation.compose.hiltViewModel
import josercl.vlcremote.compose.NavEvent

@ExperimentalLayoutApi
@ExperimentalMaterial3Api
@Composable
fun BrowseScreen(
    viewModel: BrowserViewModel = hiltViewModel(),
    onNavEvent: (NavEvent) -> Unit,
    onUpdatePlaylist: () -> Unit,
) {
    val state by viewModel.state.collectAsState()

    BrowseComposable(
        state = state,
        onNavEvent = onNavEvent,
        onBrowseEvent = viewModel::processEvent,
        onUpdatePlaylist = onUpdatePlaylist
    )
}