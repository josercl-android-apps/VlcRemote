package josercl.vlcremote.compose.features.main

import android.content.res.Configuration
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.selection.selectableGroup
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.CornerSize
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Divider
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.LocalTextStyle
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.OutlinedIconButton
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.key
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import josercl.vlcremote.compose.R
import josercl.vlcremote.compose.ui.theme.VlcRemoteTheme
import josercl.vlcremote.model.net.Subtitle

@ExperimentalMaterial3Api
@Composable
fun SubtitleActions(
    delay: Double,
    subtitles: List<Subtitle>,
    onDelayChanged: (Double) -> Unit,
    onSubtitleSelected: (Subtitle) -> Unit,
) {
    var currentDelay by remember(delay) { mutableStateOf(delay) }

    val hasted by remember(currentDelay) { derivedStateOf { currentDelay < 0 } }
    val delayed by remember(currentDelay) { derivedStateOf { currentDelay > 0 } }

    val noSubtitlesStr = key(Unit) {
        stringResource(id = R.string.subtitle_dialog_no_subtitle)
    }

    LazyColumn(Modifier.fillMaxWidth()) {
        item {
            Text(
                text = stringResource(id = R.string.subtitles_actions_title),
                style = MaterialTheme.typography.bodyMedium,
                fontWeight = FontWeight.Bold,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(all = 16.dp)
            )
            HorizontalDivider(color = MaterialTheme.colorScheme.primary)
        }
        items(
            listOf(Subtitle("", noSubtitlesStr, -1))
                    + subtitles.sorted()
        ) { subtitle ->
            Text(
                text = subtitle.language,
                style = MaterialTheme.typography.bodyLarge,
                modifier = Modifier
                    .fillMaxWidth()
                    .clickable {
                        onSubtitleSelected(subtitle)
                        //onMainControlsEvent(MainControlsEvent.SetSubtitle(subtitle))
                    }
                    .padding(all = 16.dp)
            )
        }
        item {
            HorizontalDivider(color = MaterialTheme.colorScheme.primary)
            Text(
                text = stringResource(id = R.string.subtitles_actions_sync_label),
                style = MaterialTheme.typography.bodyMedium,
                fontWeight = FontWeight.Bold,
                modifier = Modifier.padding(start = 16.dp, top = 16.dp)
            )
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(top = 16.dp),
                horizontalArrangement = Arrangement.spacedBy(
                    space = 16.dp,
                    alignment = Alignment.CenterHorizontally
                ),
                verticalAlignment = Alignment.CenterVertically
            ) {
                OutlinedIconButton(
                    onClick = {
                        currentDelay -= .5
                        onDelayChanged(currentDelay)
                    }
                ) {
                    Icon(
                        painter = painterResource(id = R.drawable.ic_left),
                        contentDescription = null,
                        modifier = Modifier.rotate(-90f)
                    )
                }
                OutlinedTextField(
                    modifier = Modifier.fillMaxWidth(.25f),
                    maxLines = 1,
                    singleLine = true,
                    textStyle = LocalTextStyle.current.copy(
                        textAlign = TextAlign.End,
                    ),
                    keyboardOptions = KeyboardOptions(
                        keyboardType = KeyboardType.Number,
                        imeAction = ImeAction.Done
                    ),
                    keyboardActions = KeyboardActions(
                        onDone = {
                            onDelayChanged(currentDelay)
                            //onMainControlsEvent(MainControlsEvent.SetSubtitleDelay(currentDelay))
                        }
                    ),
                    value = currentDelay.toString(),
                    onValueChange = {
                        currentDelay = it.filter { c ->
                            c.isDigit() || c == '-' || c == '.'
                        }.toDoubleOrNull() ?: 0.0
                    }
                )
                OutlinedIconButton(
                    onClick = {
                        currentDelay += .5
                        onDelayChanged(currentDelay)
                    }
                ) {
                    Icon(
                        painter = painterResource(id = R.drawable.ic_left),
                        contentDescription = null,
                        modifier = Modifier.rotate(90f)
                    )
                }
            }
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .selectableGroup()
                    .padding(horizontal = 16.dp, vertical = 12.dp),
                horizontalArrangement = Arrangement.Center
            ) {
                OutlinedButton(
                    modifier = Modifier.weight(1f),
                    shape = CircleShape.copy(
                        topEnd = CornerSize(0.dp),
                        bottomEnd = CornerSize(0.dp)
                    ),
                    colors = ButtonDefaults.outlinedButtonColors(
                        containerColor = when {
                            hasted -> MaterialTheme.colorScheme.secondaryContainer
                            else -> Color.Transparent
                        },
                        contentColor = when {
                            hasted -> MaterialTheme.colorScheme.onSecondaryContainer
                            else -> MaterialTheme.colorScheme.primary
                        }
                    ),
                    onClick = {
                        if (currentDelay > 0) {
                            currentDelay = -currentDelay
                            onDelayChanged(currentDelay)
                            //onMainControlsEvent(MainControlsEvent.SetSubtitleDelay(currentDelay))
                        }
                    }
                ) {
                    Text(
                        text = stringResource(id = R.string.subtitles_actions_haste_label)
                    )
                }
                OutlinedButton(
                    modifier = Modifier.weight(1f),
                    shape = CircleShape.copy(
                        topStart = CornerSize(0.dp),
                        bottomStart = CornerSize(0.dp)
                    ),
                    colors = ButtonDefaults.outlinedButtonColors(
                        containerColor = when {
                            delayed -> MaterialTheme.colorScheme.secondaryContainer
                            else -> Color.Transparent
                        },
                        contentColor = when {
                            delayed -> MaterialTheme.colorScheme.onSecondaryContainer
                            else -> MaterialTheme.colorScheme.primary
                        }
                    ),
                    onClick = {
                        if (currentDelay < 0) {
                            currentDelay = -currentDelay
                            onDelayChanged(currentDelay)
                            //onMainControlsEvent(MainControlsEvent.SetSubtitleDelay(currentDelay))
                        }
                    }
                ) {
                    Text(
                        text = stringResource(id = R.string.subtitles_actions_delay_label)
                    )
                }
            }
        }
    }
}

@ExperimentalMaterial3Api
@Preview(group = "Light")
@Preview(uiMode = Configuration.UI_MODE_NIGHT_YES, group = "Dark")
@Composable
fun SubtitleActionsPreview() {
    VlcRemoteTheme {
        Surface {
            SubtitleActions(
                delay = 1.5,
                subtitles = (0..10).map {
                    Subtitle("", "Subtitle $it", it)
                },
                onDelayChanged = {},
                onSubtitleSelected = {}
            )
        }
    }
}