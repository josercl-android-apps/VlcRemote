package josercl.vlcremote.compose.features.favorites

import josercl.vlcremote.model.Favorite

sealed class FavoriteEvent {
    data class SaveFavorite(val favorite: Favorite): FavoriteEvent()
    data class DeleteFavorite(val favorite: Favorite): FavoriteEvent()
    data class SelectFavorite(val favorite: Favorite): FavoriteEvent()
}