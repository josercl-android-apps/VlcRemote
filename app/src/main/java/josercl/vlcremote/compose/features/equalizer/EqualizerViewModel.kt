package josercl.vlcremote.compose.features.equalizer

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.squareup.moshi.Moshi
import dagger.hilt.android.lifecycle.HiltViewModel
import josercl.vlcremote.CoroutineContextDispatcher
import josercl.vlcremote.model.net.Equalizer
import josercl.vlcremote.model.net.EqualizerStatus
import josercl.vlcremote.model.net.StatusResponse
import josercl.vlcremote.net.IVlcService
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

data class EqualizerState(
    val equalizer: Equalizer = Equalizer(),
    val enabled: Boolean = false
)

@HiltViewModel
class EqualizerViewModel @Inject constructor(
    moshi: Moshi,
    private val vlcService: IVlcService,
    private val contextDispatcher: CoroutineContextDispatcher,
    savedStateHandle: SavedStateHandle
) : ViewModel() {

    private val _state = MutableStateFlow(
        EqualizerState(
            equalizer = moshi.adapter(Equalizer::class.java).fromJson(savedStateHandle.get<String>("equalizer")!!) ?: Equalizer(),
            enabled = savedStateHandle.get<Int>("equalizerEnabled") == 1
        )
    )
    val state = _state.asStateFlow()

    fun processEvent(event: EqualizerEvent) {
        when (event) {
            is EqualizerEvent.SetBand -> setBand(event.band, event.value)
            is EqualizerEvent.SetPreamp -> setPreamp(event.preamp)
            is EqualizerEvent.SetPreset -> setPreset(event.preset)
            is EqualizerEvent.ToggleEqualizer -> toggleEqualizer()
        }
    }

    private fun toggleEqualizer() {
        viewModelScope.launch(contextDispatcher.IO) {
            try {
                val response = vlcService.setEqualizerEnabled(
                    when {
                        _state.value.enabled -> EqualizerStatus.DISABLED
                        else -> EqualizerStatus.ENABLED
                    }
                )
                val enabled = response.audiofilters.equalizer

                _state.update { it.copy(enabled = enabled) }
            } catch (e: Exception) {
                Timber.e(e)
            }
        }
    }

    private fun setPreset(preset: Int) = execute { vlcService.setEqualizerPreset(preset) }

    private fun setPreamp(value: Double) = execute { vlcService.setEqualizerPreamp(value) }

    private fun setBand(band: Int, value: Double) =
        execute { vlcService.setEqualizerBand(band, value) }

    private fun execute(command: suspend () -> StatusResponse) {
        viewModelScope.launch(contextDispatcher.IO) {
            try {
                val response = command()
                _state.update { it.copy(equalizer = response.equalizer) }
            } catch (e: Exception) {
                Timber.e(e)
            }
        }
    }
}
