package josercl.vlcremote.compose.features.favorites

import android.content.res.Configuration.UI_MODE_NIGHT_YES
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Divider
import androidx.compose.material3.Icon
import androidx.compose.material3.LocalContentColor
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import josercl.vlcremote.compose.R
import josercl.vlcremote.compose.ui.theme.VlcRemoteTheme
import josercl.vlcremote.model.Favorite

@Composable
fun FavoriteActions(
    favorite: Favorite,
    contentColor: Color = LocalContentColor.current,
    onChoose: (Favorite) -> Unit,
    onEdit: (Favorite) -> Unit,
    onDelete: (Favorite) -> Unit,
) {
    Column(
        modifier = Modifier.fillMaxWidth()
    ) {
        Text(
            text = favorite.alias,
            maxLines = 1,
            overflow = TextOverflow.Ellipsis,
            style = MaterialTheme.typography.bodyMedium,
            modifier = Modifier
                .fillMaxWidth()
                .padding(all = 16.dp),
            fontWeight = FontWeight.Bold,
            color = contentColor,
        )
        Divider(
            thickness = 1.dp,
            color = MaterialTheme.colorScheme.primary
        )
        Row(
            modifier = Modifier.fillMaxWidth()
        ) {
            FavoriteActionButton(
                modifier = Modifier.weight(1f),
                icon = R.drawable.ic_check,
                text = R.string.favorite_action_choose,
                contentColor = contentColor,
                onClick = { onChoose(favorite) }
            )
            FavoriteActionButton(
                modifier = Modifier.weight(1f),
                icon = R.drawable.ic_edit,
                text = R.string.favorite_action_edit,
                contentColor = contentColor,
                onClick = { onEdit(favorite) }
            )
            FavoriteActionButton(
                modifier = Modifier.weight(1f),
                icon = R.drawable.ic_delete,
                text = R.string.favorite_action_delete,
                contentColor = MaterialTheme.colorScheme.error,
                onClick = { onDelete(favorite) }
            )
        }
    }
}

@Composable
fun FavoriteActionButton(
    modifier: Modifier = Modifier,
    @DrawableRes icon: Int,
    @StringRes text: Int,
    contentColor: Color = LocalContentColor.current,
    onClick: () -> Unit,
) {
    TextButton(
        modifier = modifier,
        onClick = onClick,
        colors = ButtonDefaults.textButtonColors(
            contentColor = contentColor
        )
    ) {
        Column(
            modifier = Modifier.padding(vertical = 8.dp),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Icon(
                painter = painterResource(id = icon),
                contentDescription = "",
            )
            Text(
                text = stringResource(id = text),
                style = MaterialTheme.typography.bodyMedium
            )
        }
    }
}

@Preview
@Preview(uiMode = UI_MODE_NIGHT_YES)
@Composable
fun FavoriteActionsPreview() {
    VlcRemoteTheme {
        Surface {
            FavoriteActions(
                favorite = Favorite(0, "Favorite Alias", "", 0, ""),
                onChoose = {},
                onEdit = {},
                onDelete = {}
            )
        }
    }
}