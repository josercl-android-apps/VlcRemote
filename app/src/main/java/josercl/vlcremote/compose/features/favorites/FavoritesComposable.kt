package josercl.vlcremote.compose.features.favorites

import android.content.res.Configuration.UI_MODE_NIGHT_YES
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.statusBarsPadding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.LocalContentColor
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.ModalBottomSheet
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import josercl.vlcremote.compose.NavEvent
import josercl.vlcremote.compose.R
import josercl.vlcremote.compose.composable.VlcRemoteTopBar
import josercl.vlcremote.compose.ui.theme.VlcRemoteTheme
import josercl.vlcremote.model.Favorite

@ExperimentalLayoutApi
@ExperimentalMaterial3Api
@Composable
fun FavoritesComposable(
    favorites: List<Favorite> = emptyList(),
    onNavEvent: (NavEvent) -> Unit,
    onFavoriteEvent: (FavoriteEvent) -> Unit,
    onSettingsChanged: () -> Unit
) {
    var clickedFavorite by remember { mutableStateOf<Favorite?>(null) }

    var currentFavorite by remember { mutableStateOf<Favorite?>(null) }
    val showFormDialog by remember { derivedStateOf { currentFavorite != null } }

    var showDeleteConfirmation by remember { mutableStateOf(false) }

    val showBottomSheet by remember { derivedStateOf { clickedFavorite != null } }

    if (showFormDialog) {
        FavoriteDialog(
            currentFavorite!!,
            onConfirm = { onFavoriteEvent(FavoriteEvent.SaveFavorite(currentFavorite!!)) },
            onDismissRequest = { currentFavorite = null },
            onAliasChanged = { currentFavorite!!.alias = it },
            onServerChanged = { currentFavorite!!.server = it },
            onPortChanged = { currentFavorite!!.port = it.toIntOrNull() ?: 0 },
            onPasswordChanged = { currentFavorite!!.pass = it }
        )
    }

    if (showDeleteConfirmation) {
        AlertDialog(
            onDismissRequest = { showDeleteConfirmation = false },
            confirmButton = {
                TextButton(onClick = {
                    onFavoriteEvent(FavoriteEvent.DeleteFavorite(clickedFavorite!!))
                    clickedFavorite = null
                    showDeleteConfirmation = false
                }) {
                    Text(text = stringResource(id = R.string.ok))
                }
            },
            dismissButton = {
                TextButton(onClick = { showDeleteConfirmation = false }) {
                    Text(text = stringResource(id = R.string.cancel))
                }
            },
            title = {
                Text(text = stringResource(id = R.string.favorite_action_delete_dialog_title))
            },
            text = {
                Text(text = stringResource(id = R.string.favorite_action_delete_dialog_message))
            }
        )
    }

    if (showBottomSheet) {
        ModalBottomSheet(
            onDismissRequest = { clickedFavorite = null },
            dragHandle = null,
        ) {
            FavoriteActions(
                contentColor = MaterialTheme.colorScheme.onSurfaceVariant,
                favorite = clickedFavorite!!,
                onChoose = {
                    onFavoriteEvent(FavoriteEvent.SelectFavorite(it))
                    onSettingsChanged()
                    clickedFavorite = null
                    onNavEvent(NavEvent.GoBack)
                },
                onEdit = {
                    currentFavorite = it
                    clickedFavorite = null
                },
                onDelete = {
                    showDeleteConfirmation = true
                }
            )
        }
    }

    Scaffold(
        modifier = Modifier,
        topBar = {
            VlcRemoteTopBar(
                modifier = Modifier.statusBarsPadding(),
                title = stringResource(id = R.string.favorites_title),
                onNavigationIconClick = { onNavEvent(NavEvent.GoBack) },
            )
        },
        floatingActionButton = {
            FloatingActionButton(onClick = {
                currentFavorite = Favorite()
            }) {
                Icon(
                    painter = painterResource(id = R.drawable.ic_add),
                    contentDescription = ""
                )
            }
        }
    ) { pad ->
        LazyColumn(
            modifier = Modifier
                .fillMaxWidth()
                .padding(pad),
        ) {
            itemsIndexed(favorites) { index, fav ->
                if (index > 0) {
                    HorizontalDivider(
//                        color = MaterialTheme.colorScheme.onSurfaceVariant.copy(alpha = .15f)
                    )
                }
                FavoriteItem(
                    fav = fav,
                    onClick = { clickedFavorite = fav }
                )
            }
        }
    }
}

@Composable
private fun FavoriteItem(
    fav: Favorite,
    onClick: () -> Unit,
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .height(64.dp)
            .clickable {
                onClick()
            }
            .padding(horizontal = 16.dp),
        verticalArrangement = Arrangement.Center
    ) {
        Text(
            text = when (fav.hasAlias) {
                true -> fav.alias
                else -> "-"
            },
            modifier = Modifier.fillMaxWidth(),
            maxLines = 1,
            style = MaterialTheme.typography.bodyLarge
        )
        Text(
            text = fav.url,
            modifier = Modifier.fillMaxWidth(),
            maxLines = 1,
            style = MaterialTheme.typography.bodyMedium,
            color = LocalContentColor.current.copy(alpha = .6f)
        )
    }
}

@ExperimentalLayoutApi
@ExperimentalMaterial3Api
@Preview
@Preview(uiMode = UI_MODE_NIGHT_YES)
@Composable
fun FavoritesComposablePreview() {
    VlcRemoteTheme {
        Surface {
            FavoritesComposable(
                favorites = (0..20).map {
                    Favorite(it, "Favorite $it", "Server", 1234, "")
                },
                onNavEvent = {},
                onFavoriteEvent = {},
                onSettingsChanged = {}
            )
        }
    }
}