package josercl.vlcremote.compose.features.equalizer

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.hilt.navigation.compose.hiltViewModel
import josercl.vlcremote.compose.NavEvent

@ExperimentalLayoutApi
@ExperimentalFoundationApi
@ExperimentalMaterial3Api
@Composable
fun EqualizerScreen(
    viewModel: EqualizerViewModel = hiltViewModel(),
    onNavEvent: (NavEvent) -> Unit,
) {
    val state by viewModel.state.collectAsState()

    EqualizerComposable(
        state = state,
        onNavEvent = onNavEvent,
        onEqualizerEvent = viewModel::processEvent
    )
}