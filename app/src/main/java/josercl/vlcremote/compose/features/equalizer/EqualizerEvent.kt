package josercl.vlcremote.compose.features.equalizer

sealed class EqualizerEvent {
    object ToggleEqualizer: EqualizerEvent()
    data class SetPreset(val preset: Int): EqualizerEvent()
    data class SetPreamp(val preamp: Double): EqualizerEvent()
    data class SetBand(val band: Int, val value: Double): EqualizerEvent()
}
