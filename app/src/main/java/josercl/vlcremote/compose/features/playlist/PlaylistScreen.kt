package josercl.vlcremote.compose.features.playlist

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import josercl.vlcremote.compose.NavEvent
import josercl.vlcremote.compose.activity.main.MainState
import josercl.vlcremote.compose.activity.main.MainViewModel

@ExperimentalFoundationApi
@ExperimentalLayoutApi
@ExperimentalMaterial3Api
@Composable
fun PlaylistScreen(
    viewModel: PlaylistViewModel = hiltViewModel(),
    mainViewModel: MainViewModel,
    onNavEvent: (NavEvent) -> Unit
) {
    val state by viewModel.state.collectAsStateWithLifecycle()

    val mainState: MainState by mainViewModel.state.collectAsStateWithLifecycle()

    PlaylistComposable(
        items = state.playlist,
        mainState = mainState,
        deletedItems = state.deletedItems,
        onNavEvent = onNavEvent,
        onPlaylistEvent = viewModel::processEvent
    )
}