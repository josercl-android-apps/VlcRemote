package josercl.vlcremote.compose.features.browse

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import josercl.vlcremote.CoroutineContextDispatcher
import josercl.vlcremote.model.net.File
import josercl.vlcremote.net.IVlcService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject

data class BrowserState(
    val loading: Boolean = false,
    val history: List<String> = emptyList(),
    val files: List<File> = emptyList()
) {
    val hasFilesInHistory = history.size > 1
}

@HiltViewModel
class BrowserViewModel @Inject constructor(
    private val vlcService: IVlcService,
    private val contextDispatcher: CoroutineContextDispatcher,
    savedStateHandle: SavedStateHandle
) : ViewModel() {
    private val _state = MutableStateFlow(BrowserState())
    val state = _state.asStateFlow()

    init {
        savedStateHandle.get<String>("uri")?.let { uri ->
            openFolder(File(uri = uri))
        }
    }

    fun processEvent(event: BrowseEvent) {
        when (event) {
            is BrowseEvent.OpenFolder -> openFolder(event.file)
            is BrowseEvent.FileEvent.Enqueue -> enqueueFile(event.file.uri)
            is BrowseEvent.FileEvent.PlayNow -> selectFile(event.file.uri)
            is BrowseEvent.EnqueueFolder -> enqueueFolder(event.file.uri, event.callback)
        }
    }

    private fun openFolder(file: File) {
        val currentState = _state.value
        if (!currentState.loading) {
            if (file.name == File.PREVIOUS_FOLDER.name && currentState.hasFilesInHistory) {
                _state.update {
                    it.copy(history = it.history.dropLast(1))
                }
            } else {
                _state.update {
                    it.copy(history = it.history + file.uri)
                }
            }
            explore()
        }
    }

    private fun explore() {
        val currentState = _state.value
        
        viewModelScope.launch(contextDispatcher.IO) {
            runCatching {
                _state.update { it.copy(loading = true) }

                val folder = currentState.history.last()

                val response = vlcService.browse(folder)

                val data: List<File> = runCatching {
                    response.element.filter(this@BrowserViewModel::validFile)
                }.getOrElse {
                    Timber.e(it)
                    emptyList()
                }.sorted()

                _state.update {
                    it.copy(
                        loading = false,
                        files = data
                    )
                }
            }.onFailure { error ->
                Timber.e(error)
                _state.update {
                    it.copy(
                        loading = false,
                        files = emptyList(),
                        history = it.history.dropLast(1)
                    )
                }
            }
        }
    }

    private fun selectFile(uri: String) {
        viewModelScope.launch(contextDispatcher.IO) {
            try {
                vlcService.openFile(uri)
            } catch (e: Exception) {
                Timber.e(e)
            }
        }
    }

    private fun enqueueFile(uri: String) {
        viewModelScope.launch(contextDispatcher.IO) {
            try {
                vlcService.enqueue(uri)
            } catch (e: Exception) {
                Timber.e(e)
            }
        }
    }

    private fun enqueueFolder(folderUri: String, callback: (() -> Unit)? = null) {
        viewModelScope.launch(contextDispatcher.IO) {
            try {
                val filesToEnqueue = ArrayList<String>()
                val visited = HashSet<String>()
                val toVisit = arrayListOf(folderUri)
                while (toVisit.isNotEmpty()) {
                    val dir = toVisit.removeAt(0)
                    if (!visited.contains(dir)) {
                        val browsedDir = vlcService.browse(dir)

                        visited.add(dir)

                        browsedDir.element.forEach {
                            if (it.isVideo() || it.isAudio()) {
                                filesToEnqueue.add(it.uri)
                            }
                            if (it.isDir() && it.name != "..") {
                                toVisit.add(it.uri)
                            }
                        }
                    }
                }

                filesToEnqueue.forEach {
                    vlcService.enqueue(it)
                }

                callback?.let {
                    withContext(Dispatchers.Main) { it.invoke() }
                }
            } catch (e: Exception) {
                Timber.e(e)
            }
        }
    }

    private fun validFile(file: File) =
        file.isVideo() || file.isImage() || file.isDir() || file.isAudio()
}