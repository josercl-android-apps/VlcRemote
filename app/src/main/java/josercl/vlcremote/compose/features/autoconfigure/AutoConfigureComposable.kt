package josercl.vlcremote.compose.features.autoconfigure

import android.content.res.Configuration
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.expandVertically
import androidx.compose.animation.shrinkVertically
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.statusBarsPadding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.Divider
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.LinearProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import josercl.vlcremote.compose.NavEvent
import josercl.vlcremote.compose.R
import josercl.vlcremote.compose.composable.VlcRemoteTopBar
import josercl.vlcremote.compose.ui.theme.VlcRemoteTheme
import josercl.vlcremote.extensions.getString
import java.net.InetAddress

@ExperimentalLayoutApi
@ExperimentalMaterial3Api
@Composable
fun AutoConfigureComposable(
    state: AutoConfigureState,
    onNavEvent: (NavEvent) -> Unit,
    onAutoConfigurationEvent: (AutoConfigurationEvent) -> Unit,
    onSettingsChanged: (Boolean) -> Unit,
) {
    var currentServer by remember { mutableStateOf<InetAddress?>(null) }

    var showDialog by remember { mutableStateOf(false) }

    if (showDialog) {
        val addrStr = currentServer?.getString().orEmpty()
        val host = stringResource(id = R.string.vlc_server, addrStr)

        AlertDialog(
            onDismissRequest = { showDialog = false },
            confirmButton = {
                TextButton(
                    onClick = {
                        showDialog = false
                        onAutoConfigurationEvent(AutoConfigurationEvent.UpdateSettings(addrStr))
                        onSettingsChanged(true)
                        onNavEvent(NavEvent.GoBack)
                    }
                ) {
                    Text(text = stringResource(id = R.string.ok))
                }
            },
            dismissButton = {
                TextButton(
                    onClick = { showDialog = false }
                ) {
                    Text(text = stringResource(id = R.string.cancel))
                }
            },
            text = {
                Text(text = stringResource(R.string.autoconfigure_select_server, host))
            }
        )
    }

    Scaffold(
        modifier = Modifier,
        topBar = {
            VlcRemoteTopBar(
                modifier = Modifier.statusBarsPadding(),
                title = stringResource(id = R.string.autoconfigure_title),
                onNavigationIconClick = { onNavEvent(NavEvent.GoBack) },
            )
        }
    ) { pad ->
        Box(modifier = Modifier.fillMaxSize().padding(pad)) {
            AnimatedVisibility(
                visible = state.loading,
                enter = expandVertically(),
                exit = shrinkVertically()
            ) {
                LinearProgressIndicator(
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(8.dp),
                    color = MaterialTheme.colorScheme.secondary,
                    trackColor = MaterialTheme.colorScheme.secondary.copy(alpha = .1f),
                    progress = (state.step.toFloat() / state.max),
                )
            }

            LazyColumn(
                modifier = Modifier.fillMaxSize(),
            ) {
                itemsIndexed(state.list) { index, server ->
                    if (index > 0) {
                        HorizontalDivider(
//                            color = MaterialTheme.colorScheme.onSurfaceVariant.copy(alpha = .15f)
                        )
                    }
                    Column(
                        modifier = Modifier
                            .fillMaxWidth()
                            .clickable {
                                currentServer = server
                                showDialog = true
                            }
                            .padding(all = 16.dp)
                    ) {
                        val addrStr = server.getString()
                        val hostName: String = server.hostName
                        Text(
                            text = hostName,
                            style = MaterialTheme.typography.bodyLarge,
                            maxLines = 1,
                            modifier = Modifier.fillMaxWidth(),
                            overflow = TextOverflow.Ellipsis
                        )
                        Text(
                            text = stringResource(R.string.vlc_server, addrStr),
                            style = MaterialTheme.typography.bodySmall,
                            maxLines = 1,
                            modifier = Modifier.fillMaxWidth(),
                            overflow = TextOverflow.Ellipsis
                        )
                    }
                }
            }
            Column(
                modifier = Modifier
                    .fillMaxHeight(.75f)
                    .fillMaxWidth(),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Bottom
            ) {
                Button(
                    onClick = {
                        if (state.loading && !state.finished) {
                            onAutoConfigurationEvent(AutoConfigurationEvent.StopScan)
                        } else if (!state.loading) {
                            onAutoConfigurationEvent(AutoConfigurationEvent.StartScan)
                        }
                    }
                ) {
                    Text(
                        text = when {
                            state.loading && !state.finished -> stringResource(id = R.string.autoconfigure_stop_scan)
                            !state.loading -> stringResource(id = R.string.autoconfigure_scan_button_text)
                            else -> ""
                        }
                    )
                }
            }
        }
    }
}

@ExperimentalLayoutApi
@ExperimentalMaterial3Api
@Preview(group = "Light")
@Preview(uiMode = Configuration.UI_MODE_NIGHT_YES, group = "Dark")
@Composable
fun AutoConfigureComposablePreview() {
    VlcRemoteTheme {
        AutoConfigureComposable(
            state = AutoConfigureState(
                list = (0..5).map {
                    val arrayOf: ByteArray = byteArrayOf(
                        192.toByte(),
                        168.toByte(), 1.toByte(),
                        it.toByte()
                    )
                    InetAddress.getByAddress(arrayOf)
                },
                step = 6,
                max = 10
            ),
            onNavEvent = {},
            onAutoConfigurationEvent = {},
            onSettingsChanged = {}
        )
    }
}