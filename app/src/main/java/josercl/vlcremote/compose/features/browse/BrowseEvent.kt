package josercl.vlcremote.compose.features.browse

import josercl.vlcremote.model.net.File

sealed class BrowseEvent(val file: File) {
    class OpenFolder(folder: File) : BrowseEvent(folder)

    data class EnqueueFolder(
        val folder: File,
        val callback: (() -> Unit)? = null
    ): BrowseEvent(folder)

    sealed class FileEvent(file: File) : BrowseEvent(file) {
        class PlayNow(f: File) : FileEvent(f)
        class Enqueue(f: File) : FileEvent(f)
    }
}
