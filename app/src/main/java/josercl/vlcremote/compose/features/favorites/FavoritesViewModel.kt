package josercl.vlcremote.compose.features.favorites

import android.content.SharedPreferences
import androidx.core.content.edit
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import josercl.vlcremote.CoroutineContextDispatcher
import josercl.vlcremote.PREF_HOST
import josercl.vlcremote.PREF_PASS
import josercl.vlcremote.PREF_PORT
import josercl.vlcremote.db.repository.FavoriteRepository
import josercl.vlcremote.model.Favorite
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

@ExperimentalCoroutinesApi
@HiltViewModel
class FavoritesViewModel @Inject constructor(
    private val repo: FavoriteRepository,
    private val preferences: SharedPreferences,
    private val contextDispatcher: CoroutineContextDispatcher
) : ViewModel() {
    val favorites: Flow<List<Favorite>> = repo.getAll().flowOn(contextDispatcher.IO)

    fun processEvent(event: FavoriteEvent) {
        when(event) {
            is FavoriteEvent.SaveFavorite -> saveFavorite(event.favorite)
            is FavoriteEvent.DeleteFavorite -> deleteFavorite(event.favorite)
            is FavoriteEvent.SelectFavorite -> selectFavorite(event.favorite)
        }
    }

    private fun selectFavorite(favorite: Favorite) {
        preferences.edit(commit = true) {
            putString(PREF_HOST, favorite.server)
            putString(PREF_PORT, favorite.port.toString())
            putString(PREF_PASS, favorite.pass)
        }
    }

    private fun saveFavorite(fav: Favorite) = when {
        fav.isNew -> execute(fav, repo::save)
        else -> execute(fav, repo::update)
    }

    private fun deleteFavorite(fav: Favorite) = execute(fav, repo::delete)

    private fun execute(fav: Favorite, func: suspend (Favorite) -> Unit) {
        viewModelScope.launch(contextDispatcher.IO) {
            try {
                func(fav)
            } catch (e: Exception) {
                Timber.e(e)
            }
        }
    }
}