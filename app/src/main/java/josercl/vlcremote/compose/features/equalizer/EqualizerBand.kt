package josercl.vlcremote.compose.features.equalizer

import android.content.res.Configuration
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.LocalContentColor
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Slider
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.unit.dp
import josercl.vlcremote.compose.R
import josercl.vlcremote.compose.ui.preview.BooleanPreviewParameterProvider
import josercl.vlcremote.compose.ui.theme.VlcRemoteTheme
import josercl.vlcremote.extensions.normalize
import kotlin.math.roundToLong

@Composable
fun EqualizerBand(
    label: String,
    value: Double,
    enabled: Boolean,
    onValueChanged: (Double) -> Unit
) {
    val (bandValue, setBandValue) = remember(value, enabled) { mutableStateOf(value) }

    Row(
        modifier = Modifier.fillMaxWidth(),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Box(
            contentAlignment = Alignment.Center,
            modifier = Modifier
                .border(
                    width = 1.dp,
                    shape = RoundedCornerShape(4.dp),
                    color = MaterialTheme.colorScheme.outline
                )
        ) {
            Text(
                text = label.padStart(6, ' '),
                modifier = Modifier.padding(horizontal = 8.dp, vertical = 4.dp),
                color = LocalContentColor.current.copy(
                    alpha = when {
                        enabled -> 1f
                        else -> .38f
                    }
                )
            )
        }
        Spacer(Modifier.width(8.dp))
        Slider(
            valueRange = -20f..20f,
            modifier = Modifier.weight(1f),
            value = bandValue.toFloat(),
            onValueChange = {
                setBandValue(
                    (it.toDouble() * 10).roundToLong() / 10.0
                )
            },
            onValueChangeFinished = { onValueChanged(bandValue) },
            enabled = enabled
        )
        Text(
            text = stringResource(id = R.string.value_in_db, bandValue.normalize()),
            color = LocalContentColor.current.copy(
                alpha = when {
                    enabled -> 1f
                    else -> .38f
                }
            )
        )
    }
}

@Preview(group = "Light")
@Preview(uiMode = Configuration.UI_MODE_NIGHT_YES, group = "Dark")
@Composable
fun EqualizerBandPreview(
    @PreviewParameter(BooleanPreviewParameterProvider::class) enabled: Boolean
) {
    VlcRemoteTheme {
        Surface {
            EqualizerBand(
                label = "16 kHz",
                value = 12.0,
                onValueChanged = {},
                enabled = enabled
            )
        }
    }
}