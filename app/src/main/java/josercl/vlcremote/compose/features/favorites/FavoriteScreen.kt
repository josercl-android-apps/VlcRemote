package josercl.vlcremote.compose.features.favorites

import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.hilt.navigation.compose.hiltViewModel
import josercl.vlcremote.compose.NavEvent
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalLayoutApi
@ExperimentalMaterial3Api
@ExperimentalCoroutinesApi
@Composable
fun FavoriteScreen(
    viewModel: FavoritesViewModel = hiltViewModel(),
    onNavEvent: (NavEvent) -> Unit,
    onSettingsChanged: () -> Unit,
) {
    val favorites by viewModel.favorites.collectAsState(initial = emptyList())

    FavoritesComposable(
        favorites = favorites,
        onNavEvent = onNavEvent,
        onFavoriteEvent = viewModel::processEvent,
        onSettingsChanged
    )
}