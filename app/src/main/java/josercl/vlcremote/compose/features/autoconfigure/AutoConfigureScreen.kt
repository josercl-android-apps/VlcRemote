package josercl.vlcremote.compose.features.autoconfigure

import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.hilt.navigation.compose.hiltViewModel
import josercl.vlcremote.compose.NavEvent

@ExperimentalLayoutApi
@ExperimentalMaterial3Api
@Composable
fun AutoConfigureScreen(
    viewModel: AutoConfigureViewModel = hiltViewModel(),
    onNavEvent: (NavEvent) -> Unit,
    onSettingsChanged: (Boolean) -> Unit
) {
    val state by viewModel.state.collectAsState()

    AutoConfigureComposable(
        state = state,
        onNavEvent = onNavEvent,
        onAutoConfigurationEvent = viewModel::processEvent,
        onSettingsChanged = onSettingsChanged
    )
}