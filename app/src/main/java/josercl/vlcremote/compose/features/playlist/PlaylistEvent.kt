package josercl.vlcremote.compose.features.playlist

import josercl.vlcremote.model.net.PlaylistNode

sealed class PlaylistEvent {
    object LoadPlaylist: PlaylistEvent()
    object ClearPlaylist: PlaylistEvent()
    data class PlayNow(val item: PlaylistNode): PlaylistEvent()
    data class RemoveFromPlaylist(val item: PlaylistNode): PlaylistEvent()
    data class DeleteItems(val items: List<PlaylistNode>): PlaylistEvent()
    object RestoreDeletedItems: PlaylistEvent()
}