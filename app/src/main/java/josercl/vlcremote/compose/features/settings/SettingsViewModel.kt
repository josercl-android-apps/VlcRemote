package josercl.vlcremote.compose.features.settings

import android.content.SharedPreferences
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import josercl.androidutils.flow.preferenceFlow
import josercl.vlcremote.PREF_M3
import josercl.vlcremote.db.dao.FavoriteDao
import josercl.vlcremote.model.Favorite
import josercl.vlcremote.model.mapper.EntityDomainMapper
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject
import josercl.vlcremote.db.entity.Favorite as DbFavorite

@HiltViewModel
class SettingsViewModel @Inject constructor(
    private val dao: FavoriteDao,
    private val preferences: SharedPreferences,
    private val mapper: EntityDomainMapper<DbFavorite, Favorite>
) : ViewModel() {
    private val _favoriteSaved = MutableStateFlow<Favorite?>(null) //SingleLiveEvent<Favorite>()
    val favoriteSaved = _favoriteSaved.asStateFlow()

    private val _dynamicColorsPref = preferenceFlow(preferences, PREF_M3, false)
    val dynamicColorsPref = _dynamicColorsPref.stateIn(viewModelScope, SharingStarted.WhileSubscribed(5000), false)

    fun processEvent(event: SettingsEvent) {
        when (event) {
            is SettingsEvent.SaveFavorite -> saveToFavorite(event.favorite)
            is SettingsEvent.ResetSavedFavorite -> resetSavedFavorite()
        }
    }

    private fun saveToFavorite(favorite: Favorite) {
        viewModelScope.launch {
            dao.save(mapper.toDatabase(favorite))
            _favoriteSaved.update { favorite }
        }
    }

    private fun resetSavedFavorite() = _favoriteSaved.update { null }
}