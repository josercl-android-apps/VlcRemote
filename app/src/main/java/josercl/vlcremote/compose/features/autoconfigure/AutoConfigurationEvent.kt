package josercl.vlcremote.compose.features.autoconfigure

sealed interface AutoConfigurationEvent {
    data class UpdateWifiStatePermission(val hasPermission: Boolean) : AutoConfigurationEvent
    object StartScan : AutoConfigurationEvent
    object StopScan : AutoConfigurationEvent
    data class UpdateSettings(val addr: String) : AutoConfigurationEvent
}