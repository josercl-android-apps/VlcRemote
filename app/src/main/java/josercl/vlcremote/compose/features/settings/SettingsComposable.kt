package josercl.vlcremote.compose.features.settings

import android.content.Intent
import android.net.Uri
import android.os.Build
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatDelegate
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.selection.selectable
import androidx.compose.foundation.selection.selectableGroup
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.RadioButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Switch
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.Role
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import josercl.vlcremote.PREF_HOST
import josercl.vlcremote.PREF_PASS
import josercl.vlcremote.PREF_PORT
import josercl.vlcremote.Routes
import josercl.vlcremote.compose.BuildConfig
import josercl.vlcremote.compose.LocalPreferences
import josercl.vlcremote.compose.NavEvent
import josercl.vlcremote.compose.R
import josercl.vlcremote.compose.composable.PreferenceEditor
import josercl.vlcremote.compose.composable.VlcRemoteTopBar
import josercl.vlcremote.compose.features.favorites.FavoriteDialog
import josercl.vlcremote.compose.ui.theme.VlcRemoteTheme
import josercl.vlcremote.extensions.getHost
import josercl.vlcremote.extensions.getPassword
import josercl.vlcremote.extensions.getPort
import josercl.vlcremote.extensions.getTheme
import josercl.vlcremote.extensions.isDynamicColors
import josercl.vlcremote.extensions.setDynamicColors
import josercl.vlcremote.extensions.setTheme
import josercl.vlcremote.model.Favorite

@ExperimentalLayoutApi
@ExperimentalMaterial3Api
@Composable
fun SettingsComposable(
    onNavEvent: (NavEvent) -> Unit = {},
    onSettingsEvent: (SettingsEvent) -> Unit,
) {
    val context = LocalContext.current
    val prefs = LocalPreferences.current

    var showThemeDialog by remember { mutableStateOf(false) }

    var currentTheme by remember { mutableIntStateOf(prefs.getTheme()) }

    val themeOptions = mapOf(
        AppCompatDelegate.MODE_NIGHT_NO to stringResource(id = R.string.pref_theme_light),
        AppCompatDelegate.MODE_NIGHT_YES to stringResource(id = R.string.pref_theme_dark),
    ) + if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
        AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM to stringResource(id = R.string.pref_theme_system_default)
    } else {
        AppCompatDelegate.MODE_NIGHT_AUTO_BATTERY to stringResource(id = R.string.pref_theme_battery_saver)
    }

    if (showThemeDialog) {
        AlertDialog(
            title = { Text(text = stringResource(id = R.string.pref_theme_title)) },
            onDismissRequest = { showThemeDialog = false },
            confirmButton = { },
            dismissButton = {
                TextButton(
                    onClick = { showThemeDialog = false }
                ) {
                    Text(text = stringResource(id = R.string.cancel))
                }
            },
            text = {
                Column(modifier = Modifier.selectableGroup()) {
                    themeOptions.forEach { (key: Int, value: String) ->
                        Row(
                            modifier = Modifier
                                .fillMaxWidth()
                                .height(56.dp)
                                .selectable(
                                    selected = currentTheme == key,
                                    onClick = {
                                        currentTheme = key
                                        prefs.setTheme(key)
                                    },
                                    role = Role.RadioButton
                                ),
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            RadioButton(
                                selected = currentTheme == key,
                                onClick = {
                                    currentTheme = key
                                    prefs.setTheme(key)
                                },
                            )
                            Spacer(modifier = Modifier.width(16.dp))
                            Text(
                                text = value,
                                maxLines = 1,
                                modifier = Modifier.weight(1f)
                            )
                        }
                    }
                }
            }
        )
    }

    var currentFavorite by remember { mutableStateOf<Favorite?>(null) }
    val showFavoriteDialog by remember { derivedStateOf { currentFavorite != null } }

    if (showFavoriteDialog) {
        FavoriteDialog(
            currentFavorite!!,
            onConfirm = {
                onSettingsEvent(SettingsEvent.SaveFavorite(currentFavorite!!))
            },
            onDismissRequest = { currentFavorite = null },
            onAliasChanged = { currentFavorite!!.alias = it },
            onServerChanged = { currentFavorite!!.server = it },
            onPortChanged = { currentFavorite!!.port = it.toIntOrNull() ?: 0 },
            onPasswordChanged = { currentFavorite!!.pass = it }
        )
    }

    Scaffold(
        topBar = {
            VlcRemoteTopBar(
                title = stringResource(id = R.string.settings_title),
                onNavigationIconClick = { onNavEvent(NavEvent.GoBack) }
            )
        },
        bottomBar = {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(all = 16.dp),
                horizontalArrangement = Arrangement.SpaceBetween,
            ) {
                Row(
                    horizontalArrangement = Arrangement.spacedBy(8.dp),
                    verticalAlignment = Alignment.CenterVertically,
                    modifier = Modifier.clickable {
                        context.startActivity(
                            Intent(Intent.ACTION_VIEW).apply {
                                data = Uri.parse("https://twitter.com/rootencio")
                            }
                        )
                    }
                ) {
                    Icon(painter = painterResource(id = R.drawable.ic_twitter), contentDescription = null)
                    Text(text = stringResource(id = R.string.done_by))
                }
                Text(
                    text = stringResource(id = R.string.version_text, BuildConfig.VERSION_NAME)
                )
            }
        }
    ) { pad ->
        LazyColumn(
            modifier = Modifier
                .fillMaxWidth()
                .padding(pad)
        ) {
            item { SettingsCategoryTitle(R.string.pref_category_conn_title) }
            item {
                PreferenceEditor(
                    icon = R.drawable.ic_server,
                    title = stringResource(id = R.string.pref_server_title),
                    prefKey = PREF_HOST,
                    prefDefaultValue = stringResource(id = R.string.pref_server_default),
                )
            }
            item {
                PreferenceEditor(
                    icon = 0,
                    title = stringResource(id = R.string.pref_port_title),
                    prefKey = PREF_PORT,
                    numeric = true,
                    prefDefaultValue = stringResource(id = R.string.pref_port_default),
                )
            }
            item {
                PreferenceEditor(
                    icon = R.drawable.ic_lock,
                    title = stringResource(id = R.string.pref_pass_title),
                    prefKey = PREF_PASS,
                    password = true,
                )
            }
            item {
                RectangularButton(
                    onClick = { onNavEvent(NavEvent.GoTo(Routes.AUTO_CONF)) },
                    icon = R.drawable.ic_wrench,
                    title = stringResource(id = R.string.pref_autoconfigure_title)
                )
            }
            item {
                RectangularButton(
                    onClick = {
                        currentFavorite = Favorite(
                            0,
                            "",
                            prefs.getHost(),
                            prefs.getPort().toInt(),
                            prefs.getPassword()
                        )
                    },
                    icon = R.drawable.ic_star,
                    title = stringResource(id = R.string.pref_save_title)
                )
            }
            item {
                RectangularButton(
                    onClick = { onNavEvent(NavEvent.GoTo(Routes.TUTORIAL)) },
                    icon = R.drawable.ic_help,
                    title = stringResource(id = R.string.help_title)
                )
            }
            item {
                SettingsCategoryTitle(R.string.pref_category_appearance_title)
            }
            item {
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(56.dp)
                        .clickable {
                            showThemeDialog = true
                        }
                        .padding(horizontal = 16.dp),
                    verticalAlignment = Alignment.CenterVertically,
                ) {
                    Icon(
                        painter = painterResource(id = R.drawable.ic_palette),
                        contentDescription = ""
                    )
                    Spacer(modifier = Modifier.width(32.dp))
                    Column(
                        modifier = Modifier
                            .weight(1f)
                            .fillMaxHeight(),
                        verticalArrangement = Arrangement.Center
                    ) {
                        Text(
                            text = stringResource(id = R.string.pref_theme_title),
                            maxLines = 1,
                            overflow = TextOverflow.Ellipsis,
                            modifier = Modifier.fillMaxWidth(),
                            style = MaterialTheme.typography.bodyLarge
                        )
                        Text(
                            text = stringResource(
                                when (currentTheme) {
                                    AppCompatDelegate.MODE_NIGHT_NO -> R.string.pref_theme_light
                                    AppCompatDelegate.MODE_NIGHT_YES -> R.string.pref_theme_dark
                                    AppCompatDelegate.MODE_NIGHT_AUTO_BATTERY -> R.string.pref_theme_battery_saver
                                    else -> R.string.pref_theme_system_default
                                }
                            ),
                            maxLines = 1,
                            overflow = TextOverflow.Ellipsis,
                            modifier = Modifier.fillMaxWidth(),
                            style = MaterialTheme.typography.bodyMedium
                        )
                    }
                }
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                item {
                    Row(
                        modifier = Modifier
                            .fillMaxWidth()
                            .height(56.dp)
                            .clickable {
                                prefs.setDynamicColors(!prefs.isDynamicColors())
                            }
                            .padding(horizontal = 16.dp),
                        verticalAlignment = Alignment.CenterVertically,
                    ) {
                        Text(
                            text = stringResource(id = R.string.pref_dynamic_colors_title),
                            maxLines = 1,
                            overflow = TextOverflow.Ellipsis,
                            modifier = Modifier.weight(1f)
                        )
                        Spacer(modifier = Modifier.width(32.dp))
                        Switch(
                            checked = prefs.isDynamicColors(),
                            onCheckedChange = {
                                prefs.setDynamicColors(it)
                            },
                            thumbContent = {
                                Icon(
                                    modifier = Modifier.size(16.dp),
                                    painter = painterResource(
                                        id = when (prefs.isDynamicColors()) {
                                            true -> R.drawable.ic_check
                                            else -> R.drawable.ic_close
                                        }
                                    ), contentDescription = null
                                )
                            }
                        )
                    }
                }
            }
        }
    }
}

@Composable
private fun SettingsCategoryTitle(@StringRes title: Int) {
    Text(
        text = stringResource(id = title),
        style = MaterialTheme.typography.labelMedium,
        color = MaterialTheme.colorScheme.secondary,
        modifier = Modifier.padding(start = 16.dp, top = 16.dp, bottom = 16.dp)
    )
}

@Composable
private fun RectangularButton(
    onClick: () -> Unit,
    icon: Int,
    title: String
) {
    TextButton(
        shape = RectangleShape,
        onClick = onClick,
        colors = ButtonDefaults.textButtonColors(
            contentColor = MaterialTheme.colorScheme.onBackground
        ),
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 4.dp)
            .height(64.dp),
    ) {
        if (icon != 0) {
            Icon(
                painter = painterResource(id = icon),
                contentDescription = ""
            )
        } else {
            Spacer(modifier = Modifier.width(24.dp))
        }
        Spacer(modifier = Modifier.width(32.dp))
        Text(
            text = title,
            maxLines = 1,
            overflow = TextOverflow.Ellipsis,
            modifier = Modifier.weight(1f)
        )
    }
}

@ExperimentalLayoutApi
@ExperimentalMaterial3Api
@Preview
@Composable
fun SettingsComposablePreview() {
    VlcRemoteTheme {
        SettingsComposable(
            onNavEvent = {},
            onSettingsEvent = {},
        )
    }
}