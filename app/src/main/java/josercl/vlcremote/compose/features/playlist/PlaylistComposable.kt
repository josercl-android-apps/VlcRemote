@file:OptIn(ExperimentalFoundationApi::class)

package josercl.vlcremote.compose.features.playlist

import android.content.res.Configuration.UI_MODE_NIGHT_YES
import androidx.activity.compose.BackHandler
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.statusBarsPadding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.ModalBottomSheet
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Snackbar
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.SnackbarResult
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.key
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.pluralStringResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import josercl.vlcremote.Routes
import josercl.vlcremote.compose.NavEvent
import josercl.vlcremote.compose.R
import josercl.vlcremote.compose.activity.main.MainState
import josercl.vlcremote.compose.composable.PlaylistItem
import josercl.vlcremote.compose.composable.VlcRemoteTopBar
import josercl.vlcremote.compose.ui.theme.VlcRemoteTheme
import josercl.vlcremote.extensions.formatSeconds
import josercl.vlcremote.extensions.getFileIcon
import josercl.vlcremote.model.net.PlaylistNode

@ExperimentalFoundationApi
@ExperimentalLayoutApi
@ExperimentalMaterial3Api
@Composable
fun PlaylistComposable(
    items: List<PlaylistNode>,
    mainState: MainState,
    deletedItems: List<PlaylistNode>,
    onNavEvent: (NavEvent) -> Unit,
    onPlaylistEvent: (PlaylistEvent) -> Unit
) {
    val snackbarHostState = remember { SnackbarHostState() }

    var showClearConfirmationDialog by remember { mutableStateOf(false) }

    var showBottomSheet by remember { mutableStateOf(false) }

    if (showClearConfirmationDialog) {
        AlertDialog(
            onDismissRequest = { showClearConfirmationDialog = false },
            confirmButton = {
                TextButton(onClick = {
                    onPlaylistEvent(PlaylistEvent.ClearPlaylist)
                    showClearConfirmationDialog = false
                }) {
                    Text(text = stringResource(id = R.string.ok))
                }
            },
            dismissButton = {
                TextButton(onClick = {
                    showClearConfirmationDialog = false
                }) {
                    Text(text = stringResource(id = R.string.cancel))
                }
            },
            text = {
                Text(text = stringResource(id = R.string.playlist_clear))
            },
            icon = {
                Icon(
                    painter = painterResource(id = R.drawable.ic_delete),
                    contentDescription = ""
                )
            }
        )
    }

    var selectedItem by remember { mutableStateOf<PlaylistNode?>(null) }

    val currentItem = items.firstOrNull(PlaylistNode::isCurrent)

    val deletedItemsMsg: String = key(deletedItems) {
        pluralStringResource(
            id = R.plurals.playlist_deleted_items,
            count = deletedItems.size,
            deletedItems.size,
        )
    }

    val undoStr = key(Unit) { stringResource(id = R.string.undo) }

    LaunchedEffect(key1 = deletedItems) {
        if (deletedItems.isNotEmpty()) {
            val result: SnackbarResult = snackbarHostState.showSnackbar(
                deletedItemsMsg,
                undoStr
            )
            when (result) {
                SnackbarResult.ActionPerformed -> onPlaylistEvent(PlaylistEvent.RestoreDeletedItems)
                SnackbarResult.Dismissed -> onPlaylistEvent(PlaylistEvent.DeleteItems(deletedItems))
            }
        }
    }

    LaunchedEffect(key1 = mainState) {
        if (mainState.refreshPlaylist) {
            onPlaylistEvent(PlaylistEvent.LoadPlaylist)
        }
    }

    BackHandler(enabled = showBottomSheet) {
        showBottomSheet = false
    }

    if (showBottomSheet) {
        ModalBottomSheet(
            onDismissRequest = { showBottomSheet = false },
            dragHandle = null,
        ) {
            PlaylistItemActions(
                name = selectedItem?.name.orEmpty(),
                onPlayNow = {
                    selectedItem?.let { onPlaylistEvent(PlaylistEvent.PlayNow(it)) }
                    showBottomSheet = false
                },
                onRemove = {
                    selectedItem?.let {
                        onPlaylistEvent(PlaylistEvent.RemoveFromPlaylist(it))
                    }
                    showBottomSheet = false
                }
            )
        }
    }

    Scaffold(
        snackbarHost = {
            SnackbarHost(
                hostState = snackbarHostState,
                snackbar = { Snackbar(it) },
            )
        },
        topBar = {
            VlcRemoteTopBar(
                modifier = Modifier.statusBarsPadding(),
                title = stringResource(id = R.string.playlist_title),
                onNavigationIconClick = { onNavEvent(NavEvent.GoBack) },
            )
        },
        floatingActionButton = {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(start = 16.dp),
            ) {
                AnimatedVisibility(
                    visible = items.isNotEmpty(),
                    modifier = Modifier.padding(start = 16.dp),
                ) {
                    FloatingActionButton(
                        onClick = { showClearConfirmationDialog = true },
                        containerColor = MaterialTheme.colorScheme.primary,
                        contentColor = MaterialTheme.colorScheme.onPrimary
                    ) {
                        Icon(
                            painter = painterResource(id = R.drawable.ic_delete),
                            contentDescription = ""
                        )
                    }
                }
                Spacer(modifier = Modifier.weight(1f))
                FloatingActionButton(
                    onClick = {
                        showBottomSheet = false
                        onNavEvent(NavEvent.GoTo(Routes.BROWSE))
                    }
                ) {
                    Icon(
                        painter = painterResource(id = R.drawable.ic_folder_open),
                        contentDescription = ""
                    )
                }
            }
        }
    ) { pad ->
        LazyColumn(
            modifier = Modifier.fillMaxSize().padding(pad),
            contentPadding = PaddingValues(bottom = 88.dp),
        ) {
            stickyHeader(key = currentItem) {
                NowPlaying(
                    modifier = Modifier.padding(16.dp),
                    item = currentItem
                )
            }
            items(items) { item ->
                PlaylistItem(
                    item = item,
                    active = item.isCurrent,
                    onClick = {
                        selectedItem = item
                        showBottomSheet = true
                    }
                )
            }
        }
    }
}

@Composable
private fun NowPlaying(
    modifier: Modifier = Modifier,
    item: PlaylistNode?
) {
    if (item == null) {
        return
    }
    Surface(
        modifier = modifier,
        shape = RoundedCornerShape(8.dp),
        tonalElevation = 4.dp,
        border = BorderStroke(
            width = 2.dp,
            color = MaterialTheme.colorScheme.primary,
        )
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp),
            horizontalArrangement = Arrangement.spacedBy(16.dp),
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Icon(
                modifier = Modifier.size(36.dp),
                painter = painterResource(id = item.uri.getFileIcon()),
                contentDescription = "",
            )
            Column(
                modifier = Modifier.weight(1f),
                verticalArrangement = Arrangement.SpaceBetween,
            ) {
                Text(
                    text = item.name.orEmpty(),
                    maxLines = 1,
                    overflow = TextOverflow.Ellipsis,
                    style = MaterialTheme.typography.titleMedium
                )
                Text(
                    text = (item.duration?.toLong() ?: 0L).formatSeconds(),
                    maxLines = 1,
                    overflow = TextOverflow.Ellipsis,
                    style = MaterialTheme.typography.bodyMedium
                )
            }
        }
    }
}

@ExperimentalFoundationApi
@ExperimentalLayoutApi
@ExperimentalMaterial3Api
@Preview(group = "Light")
@Preview(uiMode = UI_MODE_NIGHT_YES, group = "Dark")
@Composable
fun PlaylistComposablePreview() {
    VlcRemoteTheme {
        PlaylistComposable(
            items = (1..10).map {
                PlaylistNode(
                    it,
                    emptyList(),
                    "PlaylistItem $it",
                    "",
                    "",
                    if (it == 5) "current" else null,
                    3400,
                    "item.mp4"
                )
            },
            deletedItems = emptyList(),
            onNavEvent = {},
            onPlaylistEvent = {},
            mainState = MainState()
        )
    }
}