package josercl.vlcremote.compose.ui.theme

import androidx.compose.material3.Typography
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp
import josercl.vlcremote.compose.R

// Set of Material typography styles to start with
val internalTypography = Typography(
    bodyLarge = TextStyle(
        fontFamily = FontFamily.Default,
        fontWeight = FontWeight.Normal,
        fontSize = 16.sp,
        lineHeight = 24.sp,
        letterSpacing = 0.5.sp
    )
    /* Other default text styles to override
    titleLarge = TextStyle(
        fontFamily = FontFamily.Default,
        fontWeight = FontWeight.Normal,
        fontSize = 22.sp,
        lineHeight = 28.sp,
        letterSpacing = 0.sp
    ),
    labelSmall = TextStyle(
        fontFamily = FontFamily.Default,
        fontWeight = FontWeight.Medium,
        fontSize = 11.sp,
        lineHeight = 16.sp,
        letterSpacing = 0.5.sp
    )
    */
)

private val jetbrains = FontFamily(
    Font(R.font.jetbrains_mono_regular, weight = FontWeight.Normal),
    Font(R.font.jetbrains_mono_bold, weight = FontWeight.Bold),
    Font(R.font.jetbrains_mono_medium, weight = FontWeight.Medium),
)

val Typography = internalTypography.copy(

    displaySmall = internalTypography.displaySmall.copy(fontFamily = jetbrains),
    displayMedium = internalTypography.displayMedium.copy(fontFamily = jetbrains),
    displayLarge = internalTypography.displayLarge.copy(fontFamily = jetbrains),

    headlineSmall = internalTypography.headlineSmall.copy(fontFamily = jetbrains),
    headlineMedium = internalTypography.headlineMedium.copy(fontFamily = jetbrains),
    headlineLarge = internalTypography.headlineLarge.copy(fontFamily = jetbrains),

    titleSmall = internalTypography.titleSmall.copy(fontFamily = jetbrains),
    titleMedium = internalTypography.titleMedium.copy(fontFamily = jetbrains),
    titleLarge = internalTypography.titleLarge.copy(fontFamily = jetbrains),

    bodySmall = internalTypography.bodySmall.copy(fontFamily = jetbrains),
    bodyMedium = internalTypography.bodyMedium.copy(fontFamily = jetbrains),
    bodyLarge = internalTypography.bodyLarge.copy(fontFamily = jetbrains),

    labelSmall = internalTypography.labelSmall.copy(fontFamily = jetbrains),
    labelMedium = internalTypography.labelMedium.copy(fontFamily = jetbrains),
    labelLarge = internalTypography.labelLarge.copy(fontFamily = jetbrains),
)