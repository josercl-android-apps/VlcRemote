package josercl.vlcremote.compose

import android.content.SharedPreferences
import androidx.compose.runtime.compositionLocalOf
import com.squareup.moshi.Moshi

var LocalPreferences = compositionLocalOf<SharedPreferences> {
    object : SharedPreferences {
        override fun getAll(): MutableMap<String, *> {
            return mutableMapOf<String, Any>()
        }

        override fun getString(p0: String?, p1: String?): String? = null

        override fun getStringSet(p0: String?, p1: MutableSet<String>?): MutableSet<String>? = null

        override fun getInt(p0: String?, p1: Int): Int = 0

        override fun getLong(p0: String?, p1: Long): Long = 0L

        override fun getFloat(p0: String?, p1: Float): Float = 0f

        override fun getBoolean(p0: String?, p1: Boolean): Boolean = false

        override fun contains(p0: String?): Boolean = false

        override fun edit(): SharedPreferences.Editor {
            return object: SharedPreferences.Editor {
                override fun putString(p0: String?, p1: String?): SharedPreferences.Editor {
                    return this
                }

                override fun putStringSet(p0: String?, p1: MutableSet<String>?): SharedPreferences.Editor {
                    return this
                }

                override fun putInt(p0: String?, p1: Int): SharedPreferences.Editor {
                    return this

                }

                override fun putLong(p0: String?, p1: Long): SharedPreferences.Editor {
                    return this

                }

                override fun putFloat(p0: String?, p1: Float): SharedPreferences.Editor {
                    return this
                }

                override fun putBoolean(p0: String?, p1: Boolean): SharedPreferences.Editor {
                    return this
                }

                override fun remove(p0: String?): SharedPreferences.Editor {
                    return this
                }

                override fun clear(): SharedPreferences.Editor {
                    return this
                }

                override fun commit(): Boolean {
                    return false
                }

                override fun apply() {

                }
            }
        }

        override fun registerOnSharedPreferenceChangeListener(p0: SharedPreferences.OnSharedPreferenceChangeListener?) {

        }

        override fun unregisterOnSharedPreferenceChangeListener(p0: SharedPreferences.OnSharedPreferenceChangeListener?) {

        }
    }
}
var LocalMoshi = compositionLocalOf<Moshi> {
    Moshi.Builder().build()
}