package josercl.vlcremote.extensions

import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.core.content.getSystemService

fun View.hideKeyboard() {
    val inputMethodManager =
        this.context.getSystemService<InputMethodManager>()
    inputMethodManager?.hideSoftInputFromWindow(this.windowToken, 0)
}