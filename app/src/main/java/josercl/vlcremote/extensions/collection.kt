package josercl.vlcremote.extensions

inline fun <T, reified R> Collection<T>.mapToArray(block: (T) -> R): Array<R> =
    this.map(block).toTypedArray()