package josercl.vlcremote.extensions

import android.util.Base64

fun String.hidePassword() = "\u2022".repeat(length)

fun String.encodePassword(): String =
    Base64.encodeToString(":${this.trim()}".toByteArray(), Base64.NO_WRAP)

fun String.getExtension(): String {
    val dotIndex = this.lastIndexOf(".")
    if (dotIndex != -1) {
        return this.substring(dotIndex + 1)
    }
    return this
}