package josercl.vlcremote.extensions

import androidx.annotation.DrawableRes
import josercl.vlcremote.compose.R
//import josercl.vlcremote.R
import josercl.vlcremote.model.net.File

@DrawableRes
fun File.getIcon(): Int {
    return when {
        isAudio() -> R.drawable.ic_file_type_music
        isVideo() -> R.drawable.ic_file_type_video
        isImage() -> R.drawable.ic_file_type_image
        isText() -> R.drawable.ic_file_type_text
        isDir() -> R.drawable.ic_file_type_folder
        else -> R.drawable.ic_file_type_plain
    }
}

@DrawableRes
fun String?.getFileIcon() = File(type = "file", name = this.orEmpty()).getIcon()