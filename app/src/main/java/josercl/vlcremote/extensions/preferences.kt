package josercl.vlcremote.extensions

import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.content.edit
import josercl.vlcremote.PREF_FIRST_RUN
import josercl.vlcremote.PREF_HOST
import josercl.vlcremote.PREF_M3
import josercl.vlcremote.PREF_PASS
import josercl.vlcremote.PREF_PORT
import josercl.vlcremote.PREF_THEME

private const val DEFAULT_HOST = "localhost"
private const val DEFAULT_PORT = "8080"
private const val DEFAULT_PASS = ""

fun SharedPreferences.getHost(): String = getString(PREF_HOST, DEFAULT_HOST)!!
fun SharedPreferences.getPort(): String = getString(PREF_PORT, DEFAULT_PORT)!!
fun SharedPreferences.getPassword(): String = getString(PREF_PASS, DEFAULT_PASS)!!
fun SharedPreferences.isDynamicColors(): Boolean = getBoolean(PREF_M3, false)

fun SharedPreferences.isFirstRun(): Boolean = getBoolean(PREF_FIRST_RUN, true)
fun SharedPreferences.setIsFirstRun(isFirstRun: Boolean) = edit(true) {
    putBoolean(PREF_FIRST_RUN, isFirstRun)
}

fun SharedPreferences.setDynamicColors(dynamic: Boolean) {
    edit(true) {
        putBoolean(PREF_M3, dynamic)
    }
}

fun SharedPreferences.getTheme() = getInt(PREF_THEME, AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM)
fun SharedPreferences.setTheme(theme: Int) = edit(true) {
    putInt(PREF_THEME, theme)
}