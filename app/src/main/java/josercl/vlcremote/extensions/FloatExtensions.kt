package josercl.vlcremote.extensions

import android.content.res.Resources
import android.util.TypedValue
import kotlin.math.PI

fun Float.toDp(): Float {
    return TypedValue.applyDimension(
        TypedValue.COMPLEX_UNIT_DIP,
        this,
        Resources.getSystem().displayMetrics
    )
}

fun Float.asRadians() = this * Math.PI / 180
fun Float.asDegrees() = this * 180f / PI