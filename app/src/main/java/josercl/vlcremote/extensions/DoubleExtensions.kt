package josercl.vlcremote.extensions

import kotlin.math.absoluteValue

fun Double.normalize(): String {
    val absValue = this.absoluteValue
    val str = absValue.toString().padStart(
        4 + (this >= 0.0).toInt(),
        ' '
    )
    return if (this < 0.0) {
        "-$str"
    } else {
        str
    }
}