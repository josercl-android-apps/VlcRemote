package josercl.vlcremote.extensions

import java.net.InetAddress
import java.net.InetSocketAddress
import java.net.Socket

fun IntArray.toInetAddress(): InetAddress = InetAddress.getByAddress(this.map(Int::toByte).toByteArray())

operator fun InetAddress.inc(): InetAddress {
    val newAddr = this.address.map(Byte::toPositiveInt).toIntArray()
    val canIncrement = newAddr.any { it < 255 }
    if (canIncrement) {
        var carrying: Boolean
        var index = 3
        do {
            val newVal = newAddr[index] + 1
            newAddr[index] = newVal % 256
            carrying = newVal == 256
            index--
        } while (carrying)
    }
    return newAddr.toInetAddress()
}

fun InetAddress.isServer(): Boolean {
    var isServer = false
    val testSocket = Socket()
    try {
        testSocket.connect(InetSocketAddress(this, 8080), 1000)
        if (testSocket.isConnected) {
            isServer = true
        }
        if (!testSocket.isClosed) {
            testSocket.close()
        }
    } catch (e: Exception) {
    }
    return isServer
}

fun InetAddress.getString(): String = this.address.joinToString(".") { it.toPositiveInt().toString() }