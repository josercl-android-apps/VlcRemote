package josercl.vlcremote.extensions

fun Byte.toPositiveInt(): Int = toInt() and 0xFF