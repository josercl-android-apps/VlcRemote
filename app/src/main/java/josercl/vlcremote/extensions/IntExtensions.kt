package josercl.vlcremote.extensions

import android.content.res.Resources
import android.util.DisplayMetrics
import android.util.TypedValue
import kotlin.math.roundToInt

fun Int.dpToPx(): Int {
    val metrics: DisplayMetrics = Resources.getSystem().displayMetrics
    return TypedValue.applyDimension(
        TypedValue.COMPLEX_UNIT_DIP,
        this.toFloat(),
        metrics
    ).roundToInt()
}