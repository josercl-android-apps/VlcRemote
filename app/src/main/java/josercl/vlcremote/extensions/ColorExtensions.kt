package josercl.vlcremote.extensions

import android.content.res.ColorStateList

fun Int.toColorStateList() = ColorStateList.valueOf(this)