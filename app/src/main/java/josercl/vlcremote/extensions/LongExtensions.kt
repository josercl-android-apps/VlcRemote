package josercl.vlcremote.extensions

fun Long.pad(numChars: Int, padChar: Char = '0'): String = this.toString().padStart(numChars, padChar)

fun Long.formatSeconds(): String {
    val hours = this / 3600
    var rest = this % 3600
    val minutes = rest / 60
    rest %= 60
    return "${hours.pad(2)} : ${minutes.pad(2)} : ${rest.pad(2)}"
}