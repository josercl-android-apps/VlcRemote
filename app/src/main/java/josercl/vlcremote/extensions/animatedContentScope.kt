package josercl.vlcremote.extensions

import androidx.compose.animation.AnimatedContentTransitionScope
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.spring
import androidx.compose.ui.unit.IntOffset
import androidx.navigation.NavBackStackEntry

private val animationSpec = spring<IntOffset>(stiffness = Spring.StiffnessMediumLow)

@ExperimentalAnimationApi
fun AnimatedContentTransitionScope<NavBackStackEntry>.slideFromLeft() = slideIntoContainer(
    AnimatedContentTransitionScope.SlideDirection.Right,
    animationSpec
)

@ExperimentalAnimationApi
fun AnimatedContentTransitionScope<NavBackStackEntry>.slideFromRight() = slideIntoContainer(
    AnimatedContentTransitionScope.SlideDirection.Left,
    animationSpec
)

@ExperimentalAnimationApi
fun AnimatedContentTransitionScope<NavBackStackEntry>.slideToLeft() = slideOutOfContainer(
    AnimatedContentTransitionScope.SlideDirection.Left,
    animationSpec
)

@ExperimentalAnimationApi
fun AnimatedContentTransitionScope<NavBackStackEntry>.slideToRight() = slideOutOfContainer(
    AnimatedContentTransitionScope.SlideDirection.Right,
    animationSpec
)

@ExperimentalAnimationApi
fun AnimatedContentTransitionScope<NavBackStackEntry>.slideFromBottom() = slideIntoContainer(
    AnimatedContentTransitionScope.SlideDirection.Up,
    animationSpec
)

@ExperimentalAnimationApi
fun AnimatedContentTransitionScope<NavBackStackEntry>.slideToBottom() = slideOutOfContainer(
    AnimatedContentTransitionScope.SlideDirection.Down,
    animationSpec
)

@ExperimentalAnimationApi
fun AnimatedContentTransitionScope<NavBackStackEntry>.slideFromTop() = slideIntoContainer(
    AnimatedContentTransitionScope.SlideDirection.Down,
    animationSpec
)

@ExperimentalAnimationApi
fun AnimatedContentTransitionScope<NavBackStackEntry>.slideToTop() = slideOutOfContainer(
    AnimatedContentTransitionScope.SlideDirection.Up,
    animationSpec
)