package josercl.vlcremote.model.enum

enum class VlcSortOrder(private val order: Int) {
    ASC(0),
    DESC(1);

    override fun toString(): String = "$order"
}