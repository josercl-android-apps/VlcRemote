package josercl.vlcremote.model.enum

enum class VlcCommand(private val command: String) {
    PLAY("pl_play"),
    PAUSE("pl_pause"),
    STOP("pl_stop"),
    FULLSCREEN("fullscreen"),
    RANDOM("pl_random"),
    LOOP("pl_loop"),
    DELETE("pl_delete"),
    REPEAT("pl_repeat"),
    PREV("pl_previous"),
    NEXT("pl_next"),
    SEEK("seek"),
    SORT("pl_sort"),
    VOLUME("volume"),
    SUBTITLE("subtitle_track"),
    OPEN("in_play"),
    ENQUEUE("in_enqueue"),
    ENABLE_EQ("enableeq"),
    PRESET("setpreset"),
    PREAMP("preamp"),
    EQUALIZER("equalizer"),
    SUBTITLE_DELAY("subdelay"),
    CLEAR_PLAYLIST("pl_empty");

    override fun toString(): String = this.command
}