package josercl.vlcremote.model.enum

enum class VlcSortMode(private val mode: Int) {
    NAME(1),
    AUTHOR(3),
    TRACK_NO(7);

    override fun toString(): String = "$mode"
}