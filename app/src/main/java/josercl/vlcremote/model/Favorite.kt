package josercl.vlcremote.model

import android.os.Parcel
import android.os.Parcelable

data class Favorite(
    val id: Int,
    var alias: String,
    var server: String,
    var port: Int,
    var pass: String
) : Parcelable {

    val isNew: Boolean
        get() = id == 0

    val hasAlias: Boolean
        get() = alias.isNotBlank() && alias.isNotEmpty()

    val name: String
        get() = if (this.hasAlias) this.alias else this.url

    val url get() = "http://$server:$port"

    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readInt(),
        parcel.readString() ?: ""
    )

    constructor() : this(0, "", "", 0, "")

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(alias)
        parcel.writeString(server)
        parcel.writeInt(port)
        parcel.writeString(pass)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object {
        @JvmField
        val CREATOR = object : Parcelable.Creator<Favorite> {
            override fun createFromParcel(parcel: Parcel): Favorite {
                return Favorite(parcel)
            }

            override fun newArray(size: Int): Array<Favorite?> {
                return arrayOfNulls(size)
            }
        }
    }
}