package josercl.vlcremote.model.json

import com.squareup.moshi.FromJson
import com.squareup.moshi.JsonReader
import com.squareup.moshi.ToJson
import josercl.vlcremote.extensions.getExtension
import josercl.vlcremote.model.net.AudioStream
import josercl.vlcremote.model.net.Category
import josercl.vlcremote.model.net.Subtitle
import java.util.Locale
import josercl.vlcremote.model.net.Meta as VlcMeta
import josercl.vlcremote.model.net.Stream as VlcStream

class CategoryAdapter {
    @FromJson
    fun fromJson(jsonReader: JsonReader): Category {
        val subtitles = ArrayList<Subtitle>()
        val audios = ArrayList<AudioStream>()
        var meta: VlcMeta? = null

        jsonReader.beginObject()
        while (jsonReader.hasNext()) {
            val name = jsonReader.nextName().lowercase(Locale.ROOT)
            when {
                name.startsWith(STREAM) -> {
                    parseStream(jsonReader, name)?.run {
                        if (this is Subtitle) {
                            subtitles.add(this)
                        }
                        if (this is AudioStream) {
                            audios.add(this)
                        }
                    }
                }
                name == META -> meta = parseMeta(jsonReader)
                else -> jsonReader.skipValue()
            }
        }
        jsonReader.endObject()
        return Category(subtitles, audios, meta ?: VlcMeta())
    }

    @ToJson
    fun toJson(category: Category): String = "$category"

    private fun parseMeta(jsonReader: JsonReader): VlcMeta {
        jsonReader.beginObject()
        var filename = ""
        var title = ""
        var showName = ""
        var episodeNumber = ""
        var seasonNumber = ""
        while (jsonReader.hasNext()) {
            when (jsonReader.nextName()) {
                Meta.Properties.FILENAME -> filename = jsonReader.nextString()
                Meta.Properties.TITLE -> title = jsonReader.nextString()
                Meta.Properties.SHOW_NAME -> showName = jsonReader.nextString()
                Meta.Properties.SEASON -> seasonNumber = jsonReader.nextString()
                Meta.Properties.EPISODE -> episodeNumber = jsonReader.nextString()
                else -> jsonReader.skipValue()
            }
        }
        if (title.isEmpty()) {
            title = filename.removeSuffix("." + filename.getExtension())
        }
        jsonReader.endObject()
        return VlcMeta(filename, title, showName, seasonNumber, episodeNumber)
    }

    private fun parseStream(jsonReader: JsonReader, streamName: String): VlcStream? {
        val streamIndex = streamName.lowercase(Locale.ROOT).replace("stream ", "").toInt()
        var language = ""
        var type = ""
        var codec = ""
        var channels = ""
        var bitsPerSample = 0
        var sampleRate = ""
        jsonReader.beginObject()
        while (jsonReader.hasNext()) {
            val selectName = jsonReader.selectName(
                JsonReader.Options.of(
                    Stream.Properties.CODEC,
                    Stream.Properties.LANG,
                    Stream.Properties.TYPE,
                    Stream.Properties.CHANNEL,
                    Stream.Properties.BPS,
                    Stream.Properties.SAMPLE_RATE
                )
            )
            when (selectName) {
                0 -> codec = jsonReader.nextString()
                1 -> language = jsonReader.nextString()
                2 -> type = jsonReader.nextString().lowercase(Locale.getDefault())
                3 -> channels = jsonReader.nextString()
                4 -> bitsPerSample = jsonReader.nextString().toInt()
                5 -> sampleRate = jsonReader.nextString()
                else -> jsonReader.skipValue()
            }
        }
        jsonReader.endObject()
        return when (type) {
            Stream.Type.SUBTITLE -> Subtitle(codec, language, streamIndex)
            Stream.Type.AUDIO -> AudioStream(channels, bitsPerSample, codec, sampleRate, streamIndex)
            else -> null
        }
    }

    companion object {
        const val STREAM = "stream"
        const val META = "meta"

        object Meta {
            object Properties {
                const val FILENAME = "filename"
                const val TITLE = "title"
                const val SHOW_NAME = "showName"
                const val SEASON = "seasonNumber"
                const val EPISODE = "episodeNumber"
            }
        }

        object Stream {
            object Properties {
                const val CODEC = "Codec"
                const val LANG = "Language"
                const val TYPE = "Type"
                const val CHANNEL = "Channels"
                const val BPS = "Bits_per_sample"
                const val SAMPLE_RATE = "Sample_rate"
            }

            object Type {
                const val SUBTITLE = "subtitle"
                const val AUDIO = "audio"
            }
        }
    }
}