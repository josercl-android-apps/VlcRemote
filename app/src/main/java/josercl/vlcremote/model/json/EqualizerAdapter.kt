package josercl.vlcremote.model.json

import com.squareup.moshi.FromJson
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import com.squareup.moshi.JsonReader
import com.squareup.moshi.ToJson
import josercl.vlcremote.model.net.Equalizer

@JsonClass(generateAdapter = true)
data class BandsJson(
    @field:Json(name = "band id=\"0\"") val band0: Double,
    @field:Json(name = "band id=\"1\"") val band1: Double,
    @field:Json(name = "band id=\"2\"") val band2: Double,
    @field:Json(name = "band id=\"3\"") val band3: Double,
    @field:Json(name = "band id=\"4\"") val band4: Double,
    @field:Json(name = "band id=\"5\"") val band5: Double,
    @field:Json(name = "band id=\"6\"") val band6: Double,
    @field:Json(name = "band id=\"7\"") val band7: Double,
    @field:Json(name = "band id=\"8\"") val band8: Double,
    @field:Json(name = "band id=\"9\"") val band9: Double,
)

@JsonClass(generateAdapter = true)
data class EqualizerJson(
    @field:Json(name = "preamp") val preamp: Double,
    @field:Json(name = "bands") val bands: BandsJson
)

class EqualizerAdapter {
    @FromJson
    fun fromJson(jsonReader: JsonReader): Equalizer {
        if (jsonReader.peek() == JsonReader.Token.BEGIN_ARRAY) {
            jsonReader.beginArray()
            jsonReader.endArray()
            return Equalizer()
        } else {
            jsonReader.beginObject()
            var preamp = 0.0
            val bands = DoubleArray(10) { 0.0 }
            val regex = Regex("""\d+""")
            while (jsonReader.hasNext()) {
                when (jsonReader.nextName()) {
                    "preamp" -> preamp = jsonReader.nextDouble()
                    "bands" -> {
                        jsonReader.beginObject()
                        while (jsonReader.hasNext()) {
                            val bandName = jsonReader.nextName()
                            val result = regex.find(bandName)
                            val index = result?.value?.toInt() ?: 0
                            bands[index] = jsonReader.nextDouble()
                        }
                        jsonReader.endObject()
                    }
                    else -> jsonReader.skipValue()
                }
            }
            jsonReader.endObject()
            return Equalizer(preamp, bands)
        }
    }

    @ToJson
    fun toJson(eq: Equalizer): EqualizerJson {
        return EqualizerJson(
            eq.preamp,
            BandsJson(
                eq.bands[0],
                eq.bands[1],
                eq.bands[2],
                eq.bands[3],
                eq.bands[4],
                eq.bands[5],
                eq.bands[6],
                eq.bands[7],
                eq.bands[8],
                eq.bands[9]
            )
        )
    }
}