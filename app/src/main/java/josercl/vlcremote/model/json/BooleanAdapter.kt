package josercl.vlcremote.model.json

import com.squareup.moshi.FromJson
import com.squareup.moshi.JsonReader

class BooleanAdapter {
    @FromJson
    fun fromJson(jsonReader: JsonReader): Boolean = try {
        jsonReader.nextBoolean()
    } catch (e: Exception) {
        jsonReader.nextInt() == 1
    }
}