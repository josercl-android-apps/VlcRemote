package josercl.vlcremote.model.json

import com.squareup.moshi.FromJson
import com.squareup.moshi.JsonReader
import com.squareup.moshi.ToJson
import josercl.vlcremote.model.net.AudioFilters

class AudioFiltersAdapter {
    @FromJson
    fun fromJson(jsonReader: JsonReader): AudioFilters {
        var equalizer = false
        jsonReader.beginObject()
        while (jsonReader.hasNext()) {
            jsonReader.skipName()
            if (jsonReader.nextString() == "equalizer") {
                equalizer = true
            }
        }
        jsonReader.endObject()
        return AudioFilters(equalizer)
    }

    @ToJson
    fun toJson(audioFilters: AudioFilters): String = ""
}