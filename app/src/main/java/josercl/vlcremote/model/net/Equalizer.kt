package josercl.vlcremote.model.net

import android.os.Parcel
import android.os.Parcelable

data class Equalizer(
    val preamp: Double = 0.0,
    val bands: DoubleArray = DoubleArray(10) { 0.0 },
    val presetName: String = ""
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readDouble(),
        parcel.createDoubleArray() ?: (1..10).map { 0.0 }.toDoubleArray(),
        parcel.readString() ?: ""
    )

    constructor(preamp: Double, bands: DoubleArray) : this(preamp, bands, "")

    class Presets {
        companion object {
            private val FLAT_BANDS = doubleArrayOf(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0)
            private val CLASSICAL_BANDS = doubleArrayOf(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -7.2, -7.2, -7.2, -9.6)
            private val CLUB_BANDS = doubleArrayOf(0.0, 0.0, 8.0, 5.6, 5.6, 5.6, 3.2, 0.0, 0.0, 0.0)
            private val DANCE_BANDS = doubleArrayOf(9.6, 7.2, 2.4, 0.0, 0.0, -5.6, -7.2, -7.2, 0.0, 0.0)
            private val FULL_BASS_BANDS = doubleArrayOf(-8.0, 9.6, 9.6, 5.6, 1.6, -4.0, -8.0, -10.3, -11.2, -11.2)
            private val FULL_BASS_AND_TREBLE_BANDS = doubleArrayOf(7.2, 5.6, 0.0, -7.2, -4.8, 1.6, 8.0, 11.2, 12.0, 12.0)
            private val FULL_TREBLE_BANDS = doubleArrayOf(-9.6, -9.6, -9.6, -4.0, -2.4, 11.2, 16.0, 16.0, 16.0, 16.7)
            private val HEADPHONES_BANDS = doubleArrayOf(4.8, 11.2, 5.6, -3.2, -2.4, 1.6, 4.8, 9.6, 12.8, 14.4)
            private val LARGE_HALL_BANDS = doubleArrayOf(10.3, 10.3, 5.6, 5.6, 0.0, -4.8, -4.8, -4.8, 0.0, 0.0)
            private val LIVE_BANDS = doubleArrayOf(-4.8, 0.0, 4.0, 5.6, 5.6, 5.6, 4.0, 2.4, 2.4, 2.4)
            private val PARTY_BANDS = doubleArrayOf(7.2, 7.2, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 7.2, 7.2)
            private val POP_BANDS = doubleArrayOf(-1.6, 4.8, 7.2, 8.0, 5.6, 0.0, -2.4, -2.4, -1.6, -1.6)
            private val REGGAE_BANDS = doubleArrayOf(0.0, 0.0, 0.0, -5.6, 0.0, 6.4, 6.4, 0.0, 0.0, 0.0)
            private val ROCK_BANDS = doubleArrayOf(8.0, 4.8, -5.6, -8.0, -3.2, 4.0, 8.8, 11.2, 11.2, 11.2)
            private val SKA_BANDS = doubleArrayOf(-2.4, -4.8, -4.0, 0.0, 4.0, 5.6, 8.8, 9.6, 11.2, 9.6)
            private val SOFT_BANDS = doubleArrayOf(4.8, 1.6, 0.0, -2.4, 0.0, 4.0, 8.0, 9.6, 11.2, 12.0)
            private val SOFT_ROCK_BANDS = doubleArrayOf(4.0, 4.0, 2.4, 0.0, -4.0, -5.6, -3.2, 0.0, 2.4, 8.8)
            private val TECHNO_BANDS = doubleArrayOf(8.0, 5.6, 0.0, -5.6, -4.8, 0.0, 8.0, 9.6, 9.6, 8.8)

            @JvmStatic
            private val presets = listOf(
                Equalizer(12.0, FLAT_BANDS, "Flat"),
                Equalizer(12.0, CLASSICAL_BANDS, "Classical"),
                Equalizer(6.0, CLUB_BANDS, "Club"),
                Equalizer(5.0, DANCE_BANDS, "Dance"),
                Equalizer(5.0, FULL_BASS_BANDS, "Full Bass"),
                Equalizer(4.0, FULL_BASS_AND_TREBLE_BANDS, "Full Bass and Treble"),
                Equalizer(3.0, FULL_TREBLE_BANDS, "Full Treble"),
                Equalizer(4.0, HEADPHONES_BANDS, "Headphones"),
                Equalizer(5.0, LARGE_HALL_BANDS, "Large Hall"),
                Equalizer(7.0, LIVE_BANDS, "Live"),
                Equalizer(6.0, PARTY_BANDS, "Party"),
                Equalizer(6.0, POP_BANDS, "Pop"),
                Equalizer(8.0, REGGAE_BANDS, "Reggae"),
                Equalizer(5.0, ROCK_BANDS, "Rock"),
                Equalizer(6.0, SKA_BANDS, "Ska"),
                Equalizer(5.0, SOFT_BANDS, "Soft"),
                Equalizer(7.0, SOFT_ROCK_BANDS, "Soft Rock"),
                Equalizer(5.0, TECHNO_BANDS, "Techno")
            )

            fun getAll() = presets
        }
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeDouble(preamp)
        parcel.writeDoubleArray(bands)
        parcel.writeString(presetName)
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Equalizer

        if (preamp != other.preamp) return false
        if (!bands.contentEquals(other.bands)) return false
        if (presetName != other.presetName) return false

        return true
    }

    override fun hashCode(): Int {
        var result = preamp.hashCode()
        result = 31 * result + bands.contentHashCode()
        result = 31 * result + presetName.hashCode()
        return result
    }

    companion object {
        val BANDS = listOf(
            "Preamp",
            "60 Hz", "170 Hz", "310 Hz", "600 Hz", "1 kHz", "3 kHz", "6 kHz", "12 kHz", "14 kHz", "16 kHz"
        )

        @JvmField
        val CREATOR = object : Parcelable.Creator<Equalizer> {
            override fun createFromParcel(parcel: Parcel): Equalizer {
                return Equalizer(parcel)
            }

            override fun newArray(size: Int): Array<Equalizer?> {
                return arrayOfNulls(size)
            }
        }
    }
}
