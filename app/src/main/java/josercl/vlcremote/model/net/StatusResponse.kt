package josercl.vlcremote.model.net

import android.os.Parcel
import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import josercl.vlcremote.extensions.formatSeconds

@JsonClass(generateAdapter = true)
data class StatusResponse(
    @field:Json(name = "rate") val rate: Double = 1.0,
    @field:Json(name = "currentplid") val currentplid: Int = 0,
    @field:Json(name = "apiversion") val apiVersion: Int = -1,
    @field:Json(name = "length") val length: Long = 0L,
    @field:Json(name = "fullscreen") val fullscreen: Boolean = false,
    @field:Json(name = "time") val time: Long = 0L,
    @field:Json(name = "random") val random: Boolean = false,
    @field:Json(name = "position") val position: Double = 0.0,
    @field:Json(name = "repeat") val repeat: Boolean = false,
    @field:Json(name = "audiodelay") val audioDelay: Double = 0.0,
    @field:Json(name = "volume") val volume: Int = 0,
    @field:Json(name = "subtitledelay") val subtitleDelay: Double = 0.0,
    @field:Json(name = "state") val state: String = "",
    @field:Json(name = "videoeffects") val videoEffects: VideoEffects = VideoEffects(),
    @field:Json(name = "loop") val loop: Boolean = false,
    @field:Json(name = "version") val version: String = "",
    @field:Json(name = "information") val information: Information = Information(),
    @field:Json(name = "equalizer") val equalizer: Equalizer = Equalizer(),
    @field:Json(name = "audiofilters") val audiofilters: AudioFilters = AudioFilters()
) : Parcelable {

    val isPlaying: Boolean
        get() = state == "playing"
    val isStopped: Boolean
        get() = state == "stopped"

    fun getFormattedCurrentTime(): String = time.formatSeconds()
    fun getFormattedTotalTime(): String = length.formatSeconds()

    val hasSubtitles: Boolean
        get() = information.category.subtitles.isNotEmpty()

    val meta: Meta
        get() = information.category.meta

    constructor(parcel: Parcel) : this(
        parcel.readDouble(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readLong(),
        parcel.readByte() != 0.toByte(),
        parcel.readLong(),
        parcel.readByte() != 0.toByte(),
        parcel.readDouble(),
        parcel.readByte() != 0.toByte(),
        parcel.readDouble(),
        parcel.readInt(),
        parcel.readDouble(),
        parcel.readString().orEmpty(),
        parcel.readParcelable(VideoEffects::class.java.classLoader) ?: VideoEffects(),
        parcel.readByte() != 0.toByte(),
        parcel.readString().orEmpty(),
        parcel.readParcelable(Information::class.java.classLoader) ?: Information(),
        parcel.readParcelable(Equalizer::class.java.classLoader) ?: Equalizer(),
        parcel.readParcelable(AudioFilters::class.java.classLoader) ?: AudioFilters()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeDouble(rate)
        parcel.writeInt(currentplid)
        parcel.writeInt(apiVersion)
        parcel.writeLong(length)
        parcel.writeByte(if (fullscreen) 1 else 0)
        parcel.writeLong(time)
        parcel.writeByte(if (random) 1 else 0)
        parcel.writeDouble(position)
        parcel.writeByte(if (repeat) 1 else 0)
        parcel.writeDouble(audioDelay)
        parcel.writeInt(volume)
        parcel.writeDouble(subtitleDelay)
        parcel.writeString(state)
        parcel.writeParcelable(videoEffects, flags)
        parcel.writeByte(if (loop) 1 else 0)
        parcel.writeString(version)
        parcel.writeParcelable(information, flags)
        parcel.writeParcelable(equalizer, flags)
        parcel.writeParcelable(audiofilters, flags)
    }

    override fun describeContents(): Int = 0

    companion object {
        @JvmField
        val CREATOR = object : Parcelable.Creator<StatusResponse> {
            override fun createFromParcel(parcel: Parcel): StatusResponse {
                return StatusResponse(parcel)
            }

            override fun newArray(size: Int): Array<StatusResponse?> {
                return arrayOfNulls(size)
            }
        }
        const val MAX_VOLUME = 256
    }

}