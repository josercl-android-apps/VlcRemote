package josercl.vlcremote.model.net

import android.os.Parcel
import android.os.Parcelable

data class Meta(
    val filename: String = "",
    val title: String = "",
    val showName: String = "",
    val seasonNumber: String = "",
    val episodeNumber: String = ""
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString().orEmpty(),
        parcel.readString().orEmpty(),
        parcel.readString().orEmpty(),
        parcel.readString().orEmpty(),
        parcel.readString().orEmpty()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(filename)
        parcel.writeString(title)
        parcel.writeString(showName)
        parcel.writeString(seasonNumber)
        parcel.writeString(episodeNumber)
    }

    override fun describeContents(): Int = 0

    companion object CREATOR : Parcelable.Creator<Meta> {
        override fun createFromParcel(parcel: Parcel): Meta =
            Meta(parcel)

        override fun newArray(size: Int): Array<Meta?> = arrayOfNulls(size)
    }

}