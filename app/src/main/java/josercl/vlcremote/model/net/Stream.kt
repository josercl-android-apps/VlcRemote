package josercl.vlcremote.model.net

import android.os.Parcel
import android.os.Parcelable

sealed class Stream

data class AudioStream(
    val channels: String,
    val bitsPerSample: Int,
    val codec: String,
    val sampleRate: String,
    val stream: Int
) : Stream(), Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString() ?: "",
        parcel.readInt(),
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readInt()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(channels)
        parcel.writeInt(bitsPerSample)
        parcel.writeString(codec)
        parcel.writeString(sampleRate)
        parcel.writeInt(stream)
    }

    override fun describeContents(): Int = 0

    companion object CREATOR : Parcelable.Creator<AudioStream> {
        override fun createFromParcel(parcel: Parcel): AudioStream =
            AudioStream(parcel)

        override fun newArray(size: Int): Array<AudioStream?> = arrayOfNulls(size)
    }
}

data class Subtitle(
    val codec: String,
    val language: String,
    val stream: Int
) : Stream(), Parcelable, Comparable<Subtitle> {
    constructor(parcel: Parcel) : this(
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readInt()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(codec)
        parcel.writeString(language)
        parcel.writeInt(stream)
    }

    override fun compareTo(other: Subtitle): Int {
        if (stream == -1) return -1

        if (other.stream == -1) return 1

        if (language == other.language) return stream.compareTo(other.stream)

        return language.compareTo(other.language)
    }

    override fun describeContents(): Int = 0

    companion object CREATOR : Parcelable.Creator<Subtitle> {
        override fun createFromParcel(parcel: Parcel): Subtitle =
            Subtitle(parcel)

        override fun newArray(size: Int): Array<Subtitle?> = arrayOfNulls(size)
    }
}