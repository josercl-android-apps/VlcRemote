package josercl.vlcremote.model.net

import android.os.Parcel
import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class VideoEffects(
    @field:Json(name = "saturation") val saturation: Double = 0.0,
    @field:Json(name = "gamma") val gamma: Double = 0.0,
    @field:Json(name = "contrast") val contrast: Double = 0.0,
    @field:Json(name = "hue") val hue: Double = 0.0,
    @field:Json(name = "brightness") val brightness: Double = 0.0
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readDouble(),
        parcel.readDouble(),
        parcel.readDouble(),
        parcel.readDouble(),
        parcel.readDouble()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeDouble(saturation)
        parcel.writeDouble(gamma)
        parcel.writeDouble(contrast)
        parcel.writeDouble(hue)
        parcel.writeDouble(brightness)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<VideoEffects> {
        override fun createFromParcel(parcel: Parcel): VideoEffects {
            return VideoEffects(parcel)
        }

        override fun newArray(size: Int): Array<VideoEffects?> {
            return arrayOfNulls(size)
        }
    }
}