package josercl.vlcremote.model.net

import android.os.Parcel
import android.os.Parcelable
import androidx.recyclerview.widget.DiffUtil
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import josercl.vlcremote.Extensions
import josercl.vlcremote.extensions.getExtension

@JsonClass(generateAdapter = true)
data class File(
    @field:Json(name = "type") val type: String = "",
    @field:Json(name = "uri") val uri: String = "",
    @field:Json(name = "size") val size: Long = 0L,
    @field:Json(name = "name") val name: String = "",
    @field:Json(name = "path") val path: String = ""
) : Comparable<File>, Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readLong(),
        parcel.readString() ?: "",
        parcel.readString() ?: ""
    )

    private fun extension(): String = name.getExtension().lowercase()

    private fun isFile(): Boolean = type == "file"

    fun isDir(): Boolean = type == "dir"

    fun isVideo(): Boolean = isFile() && extension() in Extensions.VIDEO

    fun isImage(): Boolean = isFile() && extension() in Extensions.IMAGE

    fun isText(): Boolean = isFile() && extension() in Extensions.TEXT

    fun isAudio(): Boolean = isFile() && extension() in Extensions.AUDIO

    override fun compareTo(other: File): Int = when {
        this.isDir() && other.isFile() -> -1
        this.isVideo() && other.isFile() && !other.isVideo() -> -1
        this.isFile() && !this.isVideo() && other.isVideo() -> 1
        this.isDir() && other.isDir() ||
                this.isVideo() && other.isVideo() ||
                this.isImage() && other.isImage() ||
                this.isAudio() && other.isAudio() ||
                this.isText() && other.isText() -> this.name.compareTo(other.name)
        else -> this.name.compareTo(other.name)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(type)
        parcel.writeString(uri)
        parcel.writeLong(size)
        parcel.writeString(name)
        parcel.writeString(path)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object {
        @JvmField val CREATOR = object : Parcelable.Creator<File> {
            override fun createFromParcel(parcel: Parcel): File {
                return File(parcel)
            }

            override fun newArray(size: Int): Array<File?> {
                return arrayOfNulls(size)
            }
        }

        val PREVIOUS_FOLDER = File(name = "..")

        val DIFF_UTIL = object : DiffUtil.ItemCallback<File>() {
            override fun areItemsTheSame(oldItem: File, newItem: File): Boolean = oldItem.uri == newItem.uri

            override fun areContentsTheSame(oldItem: File, newItem: File): Boolean = oldItem == newItem
        }
    }
}