package josercl.vlcremote.model.net

import android.os.Parcel
import android.os.Parcelable

data class Category(
    val subtitles: List<Subtitle> = emptyList(),
    val audioStreams: List<AudioStream> = emptyList(),
    val meta: Meta = Meta()
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.createTypedArrayList(Subtitle) ?: emptyList(),
        parcel.createTypedArrayList(AudioStream) ?: emptyList(),
        parcel.readParcelable(Meta::class.java.classLoader) ?: Meta()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeTypedList(subtitles)
        parcel.writeTypedList(audioStreams)
        parcel.writeParcelable(meta, flags)
    }

    override fun describeContents(): Int = 0

    companion object CREATOR : Parcelable.Creator<Category> {
        override fun createFromParcel(parcel: Parcel): Category = Category(parcel)

        override fun newArray(size: Int): Array<Category?> = arrayOfNulls(size)
    }

}
