package josercl.vlcremote.model.net

import android.os.Parcel
import android.os.Parcelable

data class AudioFilters(
    val equalizer: Boolean = false
) : Parcelable {
    constructor(parcel: Parcel) : this(parcel.readByte() != 0.toByte())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeByte(if (equalizer) 1 else 0)
    }

    override fun describeContents(): Int = 0

    companion object CREATOR : Parcelable.Creator<AudioFilters> {
        override fun createFromParcel(parcel: Parcel): AudioFilters =
            AudioFilters(parcel)

        override fun newArray(size: Int): Array<AudioFilters?> = arrayOfNulls(size)
    }
}