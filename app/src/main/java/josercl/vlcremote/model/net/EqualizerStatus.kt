package josercl.vlcremote.model.net

enum class EqualizerStatus {
    DISABLED,
    ENABLED;

    override fun toString(): String = "$ordinal"
}