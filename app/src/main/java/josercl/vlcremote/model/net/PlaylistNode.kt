package josercl.vlcremote.model.net

import android.os.Parcel
import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import josercl.vlcremote.extensions.formatSeconds

@JsonClass(generateAdapter = true)
data class PlaylistNode(
    @field:Json(name = "id") val id: Int,
    @field:Json(name = "children") val children: List<PlaylistNode>?,
    @field:Json(name = "name") val name: String,
    @field:Json(name = "type") val type: String,
    @field:Json(name = "ro") val ro: String,

    @field:Json(name = "current") val current: String?,
    @field:Json(name = "duration") val duration: Int?,
    @field:Json(name = "uri") val uri: String?
) : Parcelable {
    var position: Int = -1

    val formattedDuration: String
        get() = (this.duration?.toLong() ?: 0).formatSeconds()

    val isCurrent: Boolean
        get() = this.current == "current"

    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.createTypedArrayList(CREATOR),
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString(),
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readString()
    ) {
        position = parcel.readInt()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeTypedList(children)
        parcel.writeString(name)
        parcel.writeString(type)
        parcel.writeString(ro)
        parcel.writeString(current)
        parcel.writeValue(duration)
        parcel.writeString(uri)
        parcel.writeInt(position)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PlaylistNode> {
        override fun createFromParcel(parcel: Parcel): PlaylistNode {
            return PlaylistNode(parcel)
        }

        override fun newArray(size: Int): Array<PlaylistNode?> {
            return arrayOfNulls(size)
        }
    }
}