package josercl.vlcremote.model.net

import android.os.Parcel
import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Information(
    @field:Json(name = "chapter") val chapter: Int = 0,
    @field:Json(name = "title") val title: Int = 0,
    @field:Json(name = "category") val category: Category = Category()
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readInt(),
        parcel.readParcelable(Category::class.java.classLoader) ?: Category()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(chapter)
        parcel.writeInt(title)
        parcel.writeParcelable(category, flags)
    }

    override fun describeContents(): Int = 0

    companion object CREATOR : Parcelable.Creator<Information> {
        override fun createFromParcel(parcel: Parcel): Information =
            Information(parcel)

        override fun newArray(size: Int): Array<Information?> = arrayOfNulls(size)
    }
}