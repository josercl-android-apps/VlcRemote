package josercl.vlcremote.model.mapper

import josercl.vlcremote.model.Favorite
import josercl.vlcremote.db.entity.Favorite as DbFavorite

class FavoriteMapper : EntityDomainMapper<DbFavorite, Favorite> {
    override fun toDomain(dbEntity: DbFavorite): Favorite {
        return Favorite(
            dbEntity.id,
            dbEntity.alias,
            dbEntity.server,
            dbEntity.port,
            dbEntity.pass
        )
    }

    override fun toDatabase(domainEntity: Favorite): DbFavorite {
        return DbFavorite(
            domainEntity.id,
            domainEntity.alias,
            domainEntity.server,
            domainEntity.port,
            domainEntity.pass
        )
    }
}