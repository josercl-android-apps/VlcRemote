package josercl.vlcremote.model.mapper

interface EntityDomainMapper<DBEntity, DomainEntity> {
    fun toDomain(dbEntity: DBEntity): DomainEntity
    fun toDatabase(domainEntity: DomainEntity): DBEntity
}