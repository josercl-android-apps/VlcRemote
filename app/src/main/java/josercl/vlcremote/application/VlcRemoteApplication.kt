package josercl.vlcremote.compose.application

import android.Manifest
import android.annotation.SuppressLint
import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import android.os.Build
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.content.PermissionChecker
import androidx.core.content.getSystemService
import androidx.preference.PreferenceManager
import dagger.hilt.android.HiltAndroidApp
import josercl.vlcremote.PREF_THEME
import josercl.vlcremote.compose.BuildConfig
import timber.log.Timber

@HiltAndroidApp
class VlcRemoteApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        initDebugLogging()

        initNotifications()

        bindToWifi()

        val preferences = PreferenceManager.getDefaultSharedPreferences(this)
        val defaultNightMode = when {
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q -> AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM
            else -> AppCompatDelegate.MODE_NIGHT_AUTO_BATTERY
        }

        val int = preferences.getInt(PREF_THEME, defaultNightMode)

        AppCompatDelegate.setDefaultNightMode(int)
    }

    @SuppressLint("MissingPermission")
    private fun bindToWifi() {
        val networkStatePermission = PermissionChecker.checkSelfPermission(
            this,
            Manifest.permission.ACCESS_NETWORK_STATE
        )
        if (networkStatePermission == PermissionChecker.PERMISSION_GRANTED) {
            Timber.d("I haz ACCESS_NETWORK_STATE permission")
            val cm = getSystemService<ConnectivityManager>()
            val request = NetworkRequest.Builder()
                .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
                .build()
            cm?.run {
                registerNetworkCallback(request, object : ConnectivityManager.NetworkCallback() {
                    override fun onAvailable(network: Network) {
                        super.onAvailable(network)
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            bindProcessToNetwork(network)
                        } else {
                            @Suppress("DEPRECATION")
                            ConnectivityManager.setProcessDefaultNetwork(network)
                        }
                    }
                })
            }
        }
    }

    private fun initNotifications() {
        val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val defaultChannel = NotificationChannel(
                "DEFAULT_CHANNEL",
                "Default Notification Channel",
                NotificationManager.IMPORTANCE_LOW
            )
            notificationManager.createNotificationChannel(defaultChannel)
        }
    }

    private fun initDebugLogging() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}